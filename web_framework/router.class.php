<?php 
/*
    Extremely simple & portable router with both apache and nginx support
    GOALs:
        - Handle REST efficiently
        - minimal use of regex for parsing
        
	To Add:
		- Built-in manager for /upload_tmp/ directory with expiring assets [no need of some cron job :D]
		- Handler for the asset folders (images, stylesheets and js)

*/

include('tplengine.inc.php');

$GLOBALS = array(
	'tpl_engine' => null,
	'current_url' => '',
	'url' => '',
	'parsed_path' => array(),
	'query' => array(),
);

class Router {
	protected $glob;
    private $routes = array();
	private $basepath = '/cloudbox'; //in production this should this be '/'
	
	/* 
     *   $method_name : Router::map 
     *   $args : (HTTP_METHOD, ROUTE, CALLBACK)
     *   $description : 
     */
    
	public function map($http_method, $pattern, $callback) {
		//$pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';
		array_push( $this->routes, array( $http_method, $pattern, $callback ) );
	}

	/*
     *   $method_name : Router::match 
     *   $args : ( VOID )
     *   $description : 
     */
	
	public function match() {
		$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];	//str_replace( $this->basepath, '/', isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/' );
	    $method = $_SERVER['REQUEST_METHOD'];

	    // split url
		$parsed_url = parse_url( $url );
		$parsed_query = [];

		//parse path
		$parsed_path = str_replace( $this->basepath, "",  $parsed_url['path'] );
		if( $parsed_path != '/' ) { //if we didn't find index page //add more post processing to link // this is dirty // needs sooo much fixing
			$parsed_path = array_values(array_filter( explode( '/',  $parsed_path ) ) );

			if($parsed_path[0][0] != '/') {
				//check always that first dir starts with /
				$parsed_path[0] = '/' . $parsed_path[0];
			}

			$parsed_path_size = strlen( $parsed_path[0] ) -1;

			if($parsed_path[0][$parsed_path_size] != '/') {
				//check always that the last char ends with /
				$parsed_path[0] = $parsed_path[0] . '/';
			}

		}

		//parse ?var= part
		if( isset( $parsed_url['query'] ) ) {
			parse_str( $parsed_url['query'], $parsed_query );
		}

		//match route
		foreach ( $this->routes as $route ) {
			$parsed_route = array_values(array_filter( explode( '/',  $route[1] ) ) );

			if( empty($parsed_route) ) {
				$parsed_route[0] = '/';
			}

			if( $parsed_route != null) {

				if( $parsed_route[0] != '/') {
					$parsed_route_first_arg = '/' . $parsed_route[0] . '/';
				} else {
					$parsed_route_first_arg = $parsed_route[0];
				}

				if (strcmp($parsed_path[0], $parsed_route_first_arg) == 0) { //	only match the first part /first_part/args/

					$valid = true;

					if( count($parsed_path) == count($parsed_route) && count($parsed_path) > 1 ) {

						for( $param = 1; $param <= count($parsed_path)-1; $param++) {

							if( $parsed_path[$param] != $parsed_route[$param] ) {
								$valid = false;
							}

						}

					}

					if ($valid) {
						//	match method
						if ($route[0] == $method) {
							if (!is_array($parsed_path))
								$parsed_path = [$parsed_path];

							global $GLOBALS;

							//$this->glob =& $GLOBALS;

							$GLOBALS['tpl_engine'] = (new TPLengine);
							$GLOBALS['current_url'] = $url;
							$GLOBALS['url'] = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $this->basepath;
							$GLOBALS['parsed_url'] = array_values($parsed_path);
							$GLOBALS['query'] = $parsed_query;

							return call_user_func( $route[2] );
						}
					}
				}

			}

		}
		
		//if we are here it means that the router did not find a mapped route
		//for the requested route
		//die( "404" );
		//header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
		die();
	}

}

?>