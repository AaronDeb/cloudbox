<?php

/* 
This is a class for an extremely lightweight template engine which supports 
replacing data point from a template and rendering that template out.
*/

class TPLengine {
    
    private $tpl;

    private $is_tpl = false;
    private $mime_type;

    public function __set_srctpl( $source ) {
        $this->tpl = $source;
    }

    public function __set_filetpl( $file, $data_points ) {
        $this->tpl = $file;

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $this->mime_type =  finfo_file($finfo, $file);
        finfo_close($finfo);

        if($data_points != NULL) {
            $file_contents = file_get_contents($file);
            $this->tpl = preg_replace_callback('/\<\{([^\{]{1,100}?)\}\>/', function($m) use($data_points) {
                return $data_points[$m[1]];
            }, $file_contents);
            $this->is_tpl = true;
        }
    }
    
    public function render() {
        if( $this->is_tpl ) {
            print( $this->tpl );
        } else {
            header('Content-Type: ' . $this->mime_type ); //. $this->mime_type
            readfile( $this->tpl );
        }

    }

}

?>