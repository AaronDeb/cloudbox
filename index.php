<?php
include 'php/db/dml.mysql.class.inc.php';
include 'php/fs/dml.flatfs.class.inc.php';
include 'web_framework/router.class.php';

include 'php/reporter.php';

include 'php/mappers/usermapper.php';
include 'php/models/user.class.php';

include 'php/mappers/foldermapper.php';
include 'php/models/folder.class.php';

include 'php/mappers/filemapper.php';
include 'php/models/file.class.php';

/* This is the front-end controller */

$rout_r = new Router();
$rout_r->map('GET', '/', function( ) {

    global $GLOBALS;
    session_start();

    if( ! isset($_SESSION['userID']) ) {

        $tpl_engine = $GLOBALS['tpl_engine'];

        $tpl_engine->__set_filetpl( 'index.html', NULL );
        $tpl_engine->render();
    } {
        header("Location:" . $GLOBALS["url"] . "/control-panel/");
    }
});

$rout_r->map('GET', '/control-panel/', function( ) {
    session_start();

    if( isset($_SESSION['userID']) ) {

        global $GLOBALS;
        $tpl_engine = $GLOBALS['tpl_engine'];

        $db = new MySqlDAO();
        $userMapper = new UserMapper($db);
        $user = $userMapper->findById($_SESSION['userID']);

        $tpl_engine->__set_filetpl('main.html', array(
            'user' => $user->getUserName()
        ));

        $tpl_engine->render();
    } else {
        (new ReportingFramework())->report(['condition' => "failure", 'message' => "This is a restricted area! Please login"]);
    }
});

$rout_r->map('GET', '/api/user/register/', function( ) {
    global $GLOBALS;
    $query = $GLOBALS['query'];

    if( isset($query["username"]) || isset($query["name"]) || isset($query["password"]) || isset($query["email"]) || isset($query["key"]) ) {
        $user = new User(null, $query["username"], $query["name"], $query["password"], false, $query["email"], $query["key"] );

        $db = new MySqlDAO();
        $userMapper = new UserMapper($db);
        $userMapper->insert($user);
        die("User registration was successful!");
    } else {
        die("Input all params!");
    }

});


$rout_r->map('GET', '/api/user/login/', function( ) {
    session_start();

    global $GLOBALS;
    $query = $GLOBALS['query'];

    if( isset($query["username"]) || isset($query["password"]) ) {

        $db = new MySqlDAO();
        $userMapper = new UserMapper($db);
        $user = $userMapper->findByUserName( $query["username"] );
        $userID = $user->getID();

        if( count($user) != 0 && isset( $userID ) && $userID != null ) {

            if( md5($query["password"]) == $user->getPassword() ) {
                $_SESSION['userID'] = $userID;
                (new ReportingFramework())->report(['condition' => "success", 'message' => $GLOBALS['url']]);
            } else {
                (new ReportingFramework())->report(['condition' => "failure", 'message' => "Wrong Password!"]);
            }

        } else {
            die("User not found!");
        }

    } else {
        die("Input all params!");
    }

});

$rout_r->map('GET', '/api/folder/get/', function( ) {
    session_start();

    global $GLOBALS;
    $query = $GLOBALS['query'];

    if( isset($query["folderParentId"]) ) {

        $db = new MySqlDAO();
        $fs = new FfsDAO();

        $folderID = (int)$query["folderParentId"];

        $folderMapper = new FolderMapper($db);
        $folder = new Folder(null, null, $folderID,  $_SESSION['userID'] );
        $foldersInCurrentFolder = $folderMapper->traverse( $folder );

        $fileMapper = new FileMapper($db, $fs);
        $filesInCurrentFolder = $fileMapper->findByFolderId($folderID);

        $mergedObjList = [];
        if( !empty($foldersInCurrentFolder) )
            $mergedObjList["folders"] = $foldersInCurrentFolder[0];

        if( !empty($filesInCurrentFolder) )
        //    $mergedObjList["files"] = $filesInCurrentFolder[0];

        (new ReportingFramework())->report(['condition' => "success", 'data' => json_encode($mergedObjList)]);
    } else {
        (new ReportingFramework())->report(['condition' => "failure", 'message' => "Input all params!"]);
    }

});

$rout_r->map('POST', '/api/file/new/', function( ) {
    session_start();

    global $GLOBALS;
    $query = $GLOBALS['query'];

    $file_data = $_FILES['file_data'];

    if( $file_data['error'] === UPLOAD_ERR_OK ) {

        $db = new MySqlDAO();
        $fs = new FfsDAO();

        $fileMapper = new FileMapper($db, $fs);

        //var_dump($query);

        function generateRandomString($length = 10) { //needs to be cleaner
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
        }

        // new unique name
        $newFileName = md5(generateRandomString());
        $userID = $_SESSION['userID'];

        //__construct($fileId, $fileName, $fileOwner, $fileSize, $fileModified, $fileMimeType, $fileMappedName, $fileEncrypted, $parentFolderID )
        $file = new File(null, $file_data['name'], $userID, $file_data['size'], filemtime($file_data['tmp_name']), $file_data['type'], $newFileName, 0, $_POST['where'] );
        $fileMapper->insert( $file );

        $fileContents = $fs->readFile( $file_data['tmp_name'] );

        $fileMapper->setFileContents( $file, $fileContents );

        (new ReportingFramework())->report(['condition' => "success"]);

    } else {
        (new ReportingFramework())->report(['condition' => "fail"]);
    }

});

$rout_r->map('GET', '/api/folder/new/', function( ) {
    session_start();

    global $GLOBALS;
    $query = $GLOBALS['query'];

    if( isset($query["name"]) || isset($query["folderParentId"]) || isset($query["userId"]) ) {

        $db = new MySqlDAO();
        $folderMapper = new FolderMapper($db);

        //__construct($folderId, $name, $folderInId, $userId)
        $folder = new Folder(null, $query["name"], $query["folderParentId"], $query["userId"] );
        $folderMapper->insert( $folder );

        die("Folder Created Successfully!");
    } else {
        die("Input all params!");
    }

});

$rout_r->map('GET', '/api/folder/rename/', function( ) {
    session_start();

    global $GLOBALS;
    $query = $GLOBALS['query'];

    if( isset($query["name"]) || isset($query["folderParentId"]) || isset($query["userId"]) ) {

        $db = new MySqlDAO();
        $folderMapper = new FolderMapper($db);

        //__construct($folderId, $name, $folderInId, $userId)
        $folder = new Folder($query["folderId"], $query["name"], $query["folderParentId"], $query["userId"] );
        $folderMapper->update( $folder );

        die("Folder Renamed Successfully!");
    } else {
        die("Input all params!");
    }

});

$rout_r->map('GET', '/api/folder/delete/', function( ) {
    session_start();

    global $GLOBALS;
    $query = $GLOBALS['query'];

    if( isset($query["folderId"]) || isset($query["name"]) || isset($query["folderParentId"]) || isset($query["userId"]) ) {

        $db = new MySqlDAO();
        $folderMapper = new FolderMapper($db);
        $folderMapper->delete( $query["folderId"] );

        die("Folder Deleted Successfully!");
    } else {
        die("Input all params!");
    }

});

$rout_r->map('GET', '/api/user/logout/', function( ) {
    session_start();

    if( isset( $_SESSION['userID'] ) ) {
        unset($_SESSION['userID']);
        (new ReportingFramework())->report(['condition' => "success", 'message' => '']);
    }

});

$rout_r->match();


?>