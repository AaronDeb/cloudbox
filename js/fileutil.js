var currentFolder = 'root';
var mode = 'myfiles';

// FEATURE Functionality
var opn = function () { 
	if(selectedRowCount() == 1) {
		var selRow = getSelectedRow();
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;
		Open(fName, fType);
	} 
};
var nwe = function () {
	// SHow dialog
	//<span style="font-size: 15px;font-family: 'Open Sans Condensed Bold';">Create New Folder</span>
	//<label>Please Input a valid folder name:</label> <br />
	//<input type="text" id="file-name" placeholder="fName">
	//<center><button id="set" name="ok" onclick="New(" + fName + ", " + fType + ", document.getElementById('file-name'));">OK</button></center>
	var container = document.createElement('div');

	var title =  document.createElement('span');
	title.style['font-size'] = '15px';
	title.style['font-family'] = 'Open Sans Condensed Bold';
	title.innerHTML = "Create New Folder";
	container.appendChild(title);

	container.appendChild(document.createElement('br'));

	var desc =  document.createElement('label');
	desc.innerHTML = "Please Input a valid folder name:";
	container.appendChild(desc);

	container.appendChild(document.createElement('br'));

	var textInp = document.createElement('input');
	textInp.style['margin-top'] = '20px';
	textInp.type = 'text';
	textInp.id = 'file-name';
	textInp.placeholder = 'NewFolder';

	var centerElem = document.createElement('center');
	var subElem = document.createElement('button');
	subElem.id = 'set';
	subElem.innerHTML = 'OK';
	
	subElem.onclick = function () { New(document.getElementById('file-name').value, 'Folder'); };

	centerElem.appendChild(textInp);
	centerElem.appendChild(document.createElement('br'));
	centerElem.appendChild(subElem);

	container.appendChild(centerElem);

	var errorSig = document.createElement('div');
	errorSig.id = 'error-action';

	centerElem.appendChild(document.createElement('br'));
	container.appendChild(errorSig);

	showDialog(container);
};
var rnm = function () {
	if(selectedRowCount() == 1) {
		var selRow = getSelectedRow();
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;
		
		var container = document.createElement('div');

		var title =  document.createElement('span');
		title.style['font-size'] = '15px';
		title.style['font-family'] = 'Open Sans Condensed Bold';
		title.innerHTML = "Rename File/Folder";
		container.appendChild(title);

		container.appendChild(document.createElement('br'));

		var desc =  document.createElement('label');
		desc.innerHTML = "Please Input a valid name:";
		container.appendChild(desc);

		container.appendChild(document.createElement('br'));

		var textInp = document.createElement('input');
		textInp.style['margin-top'] = '20px';
		textInp.type = 'text';
		textInp.id = 'file-name';
		textInp.placeholder = fName;

		var centerElem = document.createElement('center');
		var subElem = document.createElement('button');
		subElem.id = 'set';
		subElem.innerHTML = 'OK';

		subElem.onclick = function () { Rename(fName, document.getElementById('file-name').value, fType); };

		centerElem.appendChild(textInp);
		centerElem.appendChild(document.createElement('br'));
		centerElem.appendChild(subElem);

		container.appendChild(centerElem);

		var errorSig = document.createElement('div');
		errorSig.id = 'error-action';

		centerElem.appendChild(document.createElement('br'));
		container.appendChild(errorSig);

		showDialog(container);

	} 
};
var rem = function () {
	var selectedRows = new Array();
	var tbl =  document.getElementsByClassName("filebrowser-table")[0];
	var tbody = tbl.children[1];
	for(var i =0; i<= tbody.childElementCount-1; i++) {
		var checkState = tbody.children[i].childNodes[0].childNodes[0].checked;
		if(checkState == true) { 
			var selRow = tbody.children[i]; 
			//Delete(fName, fType);
			selectedRows[selectedRows.length] = selRow;
		} 
	}

	for(var v = 0; v <= selectedRows.length-1; v++) {
		var selRow = selectedRows[v]; 
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;
		Delete(fName, fType);
	}
};
var dwm = function () {
	var selectedRows = new Array();
	var tbl =  document.getElementsByClassName("filebrowser-table")[0];
	var tbody = tbl.children[1];
	for(var i =0; i<= tbody.childElementCount-1; i++) {
		var checkState = tbody.children[i].childNodes[0].childNodes[0].checked;
		if(checkState == true) { 
			var selRow = tbody.children[i]; 
			selectedRows[selectedRows.length] = selRow;
		} 
	}

	if(selectedRows.length == 1)
	{
		var selRow = selectedRows[0]; 
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;

		var onreadyF = function() { 
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				if(fType == 'Folder') {
					document.location = "temp/" + fName + ".zip";
				} else {
					document.location = "temp/" + fName;
				}
			}
		};
		submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fDownload&dmode=single&name=" + fName + "&type=" + fType + "&where=" + currentFolder, true);
		
	} else if(selectedRows.length > 1) {
		var onreadyF = function() { 
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var resp = xmlhttp.responseText;
				if (window.DOMParser)
				{
					parser=new DOMParser();
					xmlDoc=parser.parseFromString(resp,"text/xml");
					var xmlElems = xmlDoc.getElementsByTagName("key");
					if(xmlElems.length != 0) {
						var key = xmlElems[0].childNodes[0].data;
						for(var v = 0; v <= selectedRows.length-1; v++) {
							var selRow = selectedRows[v]; 
							var fName = selRow.childNodes[1].childNodes[0].textContent;
							var fType = selRow.childNodes[4].childNodes[0].textContent;
							submitRequest(null, null, "GET", "php/getff.php?mode=file&action=fDownload&dmode=package&name=" + fName + "&type=" + fType + "&where=" + currentFolder + "&key=" + key, true);
						}
						submitRequest(null, null, "GET", "php/getff.php?mode=file&action=fDownload&dmode=compress&key=" + key, true);
						document.location = "temp/" + key + ".zip";
					}
					
				}
			}
		};
		
		submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fDownload&dmode=getkey", true);
	}
};

var shr = function () {
	if(selectedRowCount() == 1) {
		var selRow = getSelectedRow();
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;
		
		var container = document.createElement('div');

		var title =  document.createElement('span');
		title.style['font-size'] = '15px';
		title.style['font-family'] = 'Open Sans Condensed Bold';
		title.innerHTML = "Share File";
		container.appendChild(title);

		container.appendChild(document.createElement('br'));

		var desc =  document.createElement('label');
		desc.innerHTML = "Please Input a valid username:";
		container.appendChild(desc);

		container.appendChild(document.createElement('br'));

		var textInp = document.createElement('input');
		textInp.style['margin-top'] = '20px';
		textInp.type = 'text';
		textInp.id = 'file-name';
		textInp.placeholder = 'Username';

		var centerElem = document.createElement('center');
		var subElem = document.createElement('button');
		subElem.id = 'set';
		subElem.innerHTML = 'OK';

		subElem.onclick = function () { Share(fName, document.getElementById('file-name').value, fType); };

		centerElem.appendChild(textInp);
		centerElem.appendChild(document.createElement('br'));
		centerElem.appendChild(subElem);

		container.appendChild(centerElem);

		var errorSig = document.createElement('div');
		errorSig.id = 'error-action';

		centerElem.appendChild(document.createElement('br'));
		container.appendChild(errorSig);

		showDialog(container);

	}
};

var ush = function () {
	if(selectedRowCount() == 1) {
		var selRow = getSelectedRow();
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;

		var onreadyF = function() { 
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var resp = xmlhttp.responseText;
				getList();
			}
		};

		submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fUnShare&name=" + fName + "&type=" + fType + "&where=" + currentFolder, true);
	}
};

var enc = function () {
	if(selectedRowCount() == 1) {
		var selRow = getSelectedRow();
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;

		var onreadyF = function() { 
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var resp = xmlhttp.responseText;
				getList();
			}
		};
		
		submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fEncrypt&name=" + fName + "&type=" + fType + "&where=" + currentFolder, true);
	}
};

var dec = function () {
	if(selectedRowCount() == 1) {
		var selRow = getSelectedRow();
		var fName = selRow.childNodes[1].childNodes[0].textContent;
		var fType = selRow.childNodes[4].childNodes[0].textContent;

		var onreadyF = function() { 
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var resp = xmlhttp.responseText;
				getList();
			}
		};
		
		submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fDecrypt&name=" + fName + "&type=" + fType + "&where=" + currentFolder, true);
	}
};
////////////////////////


function getList()
{	
	//function submitRequest(onreadyFunc, onprogressFunc, requestVar, fullRequest, showLoadProgress)
	var onreadyF = function() { 
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var resp = xmlhttp.responseText;
			var widP = ["30px", "36%", "30%", "20.3%", "15%"];
			if (window.DOMParser)
			{
				parser=new DOMParser();
				xmlDoc=parser.parseFromString(resp,"text/xml");
				var tblName =  document.getElementsByClassName("filebrowser-table");
				//Clear Table
				var tbody = tblName[0].children[1];
				tbody.innerHTML = "";
				
				var xmlElems =  xmlDoc.getElementsByTagName("file");
				for(var i =0; i<= xmlElems.length-1; i++)
				{
					var row = tblName[0].getElementsByTagName('tbody')[0].insertRow(rowCount);
					row.style['borderLeft'] = '1px solid #ddd';
					row.style['borderBottom'] = '1px solid #ddd';
					
					var cell = row.insertCell(0); //HERE ADD CHECKBOX
					
					var chkbox = document.createElement("input");
					chkbox.type = "checkbox";
					chkbox.classname = "checkbox";
					chkbox.onclick = function () { selectMe(this); };
					cell.appendChild(chkbox);
					cell.style['width'] = widP[0];
					
					for(var x =0; x<= xmlElems[i].childNodes.length-1; x++) //0,1,2,3 [4]
					{
						var globElem = document.createElement('div');
						var textNde = document.createTextNode(xmlElems[i].childNodes[x].textContent);
						if( (x+1) == 1) {
							// ADD File Icon
							var elemIMG  = document.createElement('img');
							elemIMG.src = 'images/icons/file.png'; 
							elemIMG.style.height = '20px';
							//padding-right: 5px;
							elemIMG.style.paddingRight = '5px';
							globElem.appendChild(elemIMG);
						} 
						globElem.appendChild(textNde);
						cell = row.insertCell(x+1);
						cell.appendChild(globElem);
						cell.style['width'] = widP[x+1];

					}
					
					row.onclick = function() { selectMe(this); };
				}

				var xmlElems =  xmlDoc.getElementsByTagName("folder");
				for(var i =0; i<= xmlElems.length-1; i++)
				{
					var row = tblName[0].getElementsByTagName('tbody')[0].insertRow(rowCount);
					row.style['borderLeft'] = '1px solid #ddd';
					row.style['borderBottom'] = '1px solid #ddd';
					
					var cell = row.insertCell(0); //HERE ADD CHECKBOX
					
					var chkbox = document.createElement("input");
					chkbox.type = "checkbox";
					chkbox.classname = "checkbox";
					chkbox.onclick = function () { selectMe(this); };
					cell.appendChild(chkbox);
					cell.style['width'] = widP[0];
					
					for(var x =0; x<= 3; x++) //0,1,2,3 [4]
					{
						var globElem = document.createElement('div');
						var textNde;
						if( (x+1) == 1) {
							textNde = document.createTextNode(xmlElems[i].childNodes[x].textContent);
							// ADD File Icon
							var elemIMG  = document.createElement('img');
							elemIMG.src = 'images/icons/folder.png'; 
							elemIMG.style.height = '20px';
							//elemIMG.style.width = '25px';
							//padding-right: 5px;
							elemIMG.style.paddingRight = '5px';
							globElem.appendChild(elemIMG);
						} else if( (x+1) == 2 || (x+1) == 3 ) {
							textNde = document.createTextNode(' - ');
						} else if( (x+1) == 4 ) {
							textNde = document.createTextNode('Folder');
						}
						globElem.appendChild(textNde);
						cell = row.insertCell(x+1);
						cell.appendChild(globElem);
						cell.style['width'] = widP[x+1];

					}
					
					row.onclick = function() { selectMe(this); };
				}

				var rowCount = tblName[0].getElementsByTagName('tbody')[0].rows.length;
				//First check if current folder is root
				if(currentFolder != "root") {
					//Add first row as ..
					var row = tbody.insertRow(rowCount);
					row.style['borderLeft'] = '1px solid #ddd';
					row.style['borderBottom'] = '1px solid #ddd';
					
					
					var chkbox = document.createElement("input");
					chkbox.type = "checkbox";
					chkbox.classname = "checkbox";
					chkbox.onclick = function () { goBack(this); };

					var cell = row.insertCell(0); //HERE ADD CHECKBOX
					cell.appendChild(chkbox);
					cell.style['width'] = widP[0];

					var textNde = document.createTextNode("...");
					cell = row.insertCell(1);
					cell.appendChild(textNde);	
					cell.style['width'] = widP[1];

					for(var v =2; v <= 4; v++) {
						textNde = document.createTextNode("-");
						cell = row.insertCell(v);
						cell.appendChild(textNde);	
						cell.style['width'] = widP[v];
					}	

					row.onclick = function() { goBack(this); };
				}
				
				
				
			}
		}
	}
	switch (mode) {
		case "myfiles":
			submitRequest(onreadyF, null, "GET", "php/getff.php?mode=folder&fn=" + currentFolder, true);
			break;
		case "shfiles":
			submitRequest(onreadyF, null, "GET", "php/getff.php?mode=shared", true);
			break;
		case "enfiles":
			submitRequest(onreadyF, null, "GET", "php/getff.php?mode=encrypted", true);
			break;
		default:
			//unknown(%20)corrupted mode
	}
	
}

function goBack(elem) {
	clearMenu();
	var objArray = [ ["New", function() {  nwe(); }] ];
	makeMenu(objArray);
	currentFolder = 'root';
	getList();
}

function selectMe(elem) {
	//Change color and state.
	if(elem.checked == null) {
		var checkState = elem.children[0].childNodes[0].checked;
		if(checkState == true) {
			elem.children[0].childNodes[0].checked = false;
			elem.style.backgroundColor = 'white';
			var trSelectedCount = selectedRowCount();
			if(trSelectedCount == 0) {
				clearMenu();
				var objArray = [ ["New", function() {  nwe(); }] ];
				makeMenu(objArray);
			} else if(trSelectedCount == 1) {
				//Get the other selected 
			
				var objArray;
				switch (mode) {
					case "myfiles":
						
						var selRow =  getSelectedRow();
						var type = selRow.children[4].childNodes[0].textContent;
						
						if(type == 'Folder') {
							objArray = [ 
								["New", function() { nwe(); }], 
								["Open", function() { opn(); }], 
								["Rename", function() { rnm(); }],
								["Remove", function() { rem(); }], 
								["Download", function() { dwm(); }] 
							];
						} else {
							objArray = [ 
								["New", function() {  nwe(); }], 
								["Open", function() { opn(); }], 
								["Rename", function() { rnm(); }], 
								["Encrypt", function() { enc(); }], 
								["Remove", function() { rem(); }], 
								["Share", function() { shr(); }], 
								["Download", function() { dwm(); }] 
							];						
						}
						break;
					case "shfiles":
						objArray = [ 
							["Open", function() { opn(); }], 
							["Remove", function() { rem(); }], 
							["UnShare", function() { ush(); }], 
							["Download", function() { dwm(); }] 
						];
						break;
					case "enfiles":
						objArray = [ 
							["Decrypt", function() { dec(); }], 
							["Remove", function() { rem(); }] 
						];
						break;
					default:
						//unknown(%20)corrupted mode
				}
				
				makeMenu(objArray);
			}
			
			

		} else if(checkState == false) {
			elem.children[0].childNodes[0].checked = true;
			elem.style.backgroundColor = 'aliceblue';

			//Get tbody handle and loop through the tr's
			var trSelectedCount = selectedRowCount();
			if(trSelectedCount == 1) {
				var objArray;
				switch (mode) {
					case "myfiles":
						var type = elem.children[4].childNodes[0].textContent;
						if(type == 'Folder') {
							objArray = [ 
								["New", function() {  nwe(); }], 
								["Open", function() { opn(); }], 
								["Rename", function() { rnm(); }],
								["Remove", function() { rem(); }], 
								["Download", function() { dwm(); }] 
							];
						} else {
							objArray = [ 
								["New", function() {  nwe(); }], 
								["Open", function() { opn(); }],
								["Rename", function() { rnm(); }], 
								["Encrypt", function() { enc(); }], 
								["Remove", function() { rem(); }], 
								["Share", function() { shr(); }],  
								["Download", function() { dwm(); }] 
							];						
						}
						break;
					case "shfiles":
						objArray = [ 
							["Open", function() { opn(); }], 
							["Remove", function() { rem(); }], 
							["UnShare", function() { ush(); }], 
							["Download", function() { dwm(); }] 
						];
						break;
					case "enfiles":
						objArray = [ 
							["Decrypt", function() { dec(); }], 
							["Remove", function() { rem(); }] 
						];
						break;
					default:
						//unknown(%20)corrupted mode
				}

				makeMenu(objArray);
			} else {
				var objArray = [ 
					["New", function() {  nwe(); }], 
					["Download", function() { dwm(); }], 
					["Remove", function() { rem(); }] 
				];
				makeMenu(objArray);
			}
		}
		
	} else {
		var parentElem = elem.parentElement.parentElement;
		var checkState = elem.checked;
		if(checkState == true) {
			elem.checked = false;
			parentElem.style.backgroundColor = 'white';
		} else if(checkState == false) {
			elem.checked = true;
			parentElem.style.backgroundColor = 'aliceblue';
		}
	}
}

function selectedRowCount() {
	var tbl =  document.getElementsByClassName("filebrowser-table")[0];
	var tbody = tbl.children[1];
	var selected =0;
	for(var i =0; i<= tbody.childElementCount-1; i++) {
		var checkState = tbody.children[i].childNodes[0].childNodes[0].checked;
		if(checkState == true) { selected +=1; } 
	}
	return selected;
}
function getSelectedRow() {
	var tbl =  document.getElementsByClassName("filebrowser-table")[0];
	var tbody = tbl.children[1];
	for(var i =0; i<= tbody.childElementCount-1; i++) {
		var checkState = tbody.children[i].childNodes[0].childNodes[0].checked;
		if(checkState == true) { 
			return  tbody.children[i]; 
			} 
	}
}

function selectAll(elem) {
	//background-color: aliceblue;

	var checkState = elem.checked;
	if(checkState == true) {
		//Select ALL
		var tbody = elem.parentElement.parentElement.parentElement.parentElement.children[1];
		for(var i = 0; i<=tbody.childElementCount-1; i++) {
			var child = tbody.children[i];
			child.children[0].childNodes[0].checked = true;
			child.style.backgroundColor = 'aliceblue';
		}
		var objArray = [ 
			["New", function() {  nwe(); }], 
			["Download", function() { dwm(); }], 
			["Remove", function() { rem(); }] 
		];
		makeMenu(objArray);
	} else if(checkState == false) {
		//UnSelect ALL
		var tbody = elem.parentElement.parentElement.parentElement.parentElement.children[1];
		for(var i = 0; i<=tbody.childElementCount-1; i++) {
			var child = tbody.children[i];
			child.children[0].childNodes[0].checked = false;
			child.style.backgroundColor = 'white';
			clearMenu();
			var objArray = [ ["New", function() {  nwe(); }] ];
			makeMenu(objArray);
		}
	}
}

function Open(name, type) {
	//Validate actual filename and type
	
	var type;
	//Check compatible types
	switch (type) {
		case "Folder":
			currentFolder = name;
			getList();
			break;
		case "image":
			//Get actual filename and then put it in the dialog
			var onreadyF = function() { 
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var resp = xmlhttp.responseText;
					if (window.DOMParser)
					{
						parser=new DOMParser();
						xmlDoc=parser.parseFromString(resp,"text/xml");
						var xmlElems = xmlDoc.getElementsByTagName("filecontents");
						// 
						var elemIMG  = document.createElement('img');
						elemIMG.src = "data:" + type + "/bmp;base64," + xmlElems[0].childNodes[0].data;
						//If filecontents is valid => output
						if(xmlElems[0].childNodes[0].data != "") {
							showDialog(elemIMG);
						}
					}
				}
			}

			submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fOpen&filename=" + name + "&type=image&folder=" + currentFolder, true);
			break;
		case "video":
			//Get actual filename and then put it in the dialog
			var onreadyF = function() { 
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var resp = xmlhttp.responseText;
					if (window.DOMParser)
					{
						parser=new DOMParser();
						xmlDoc=parser.parseFromString(resp,"text/xml");
						var xmlElems = xmlDoc.getElementsByTagName("videofname");
						if(xmlElems[0].childNodes[0].data != "")
						{
							var elemVIDc  = document.createElement('video');
							var elemVID  = document.createElement('source');
							//Get temporary filename and set it to play
							elemVID.src = xmlElems[0].childNodes[0].data;
							elemVID.type = "video/mp4";
							elemVIDc.appendChild(elemVID);
							elemVIDc.width = '540';
							elemVIDc.height = '430';
							elemVIDc.controls = true;
							showDialog(elemVIDc);
						}
					}
				}
			}

			submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fOpen&filename=" + name + "&type=video&folder=" + currentFolder, true);
			break;
		case "application":
			if(name.substr(name.length -4) == ".zip") {

			}
			break;
		default:
			//type not supported
	}
}

function New(fName, fType) {
	if(fType == 'Folder') {
		if(fName == '')
		{
			fName = 'NewFolder';
		}
		var onreadyF = function() { 
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var resp = xmlhttp.responseText;
				if (window.DOMParser)
				{
					parser=new DOMParser();
					xmlDoc=parser.parseFromString(resp,"text/xml");
					var xmlElems = xmlDoc.getElementsByTagName("message");
					if(xmlElems.length != 0) {
						document.getElementById("error-action").innerHTML = xmlElems[0].childNodes[0].data;
					} else { //success
						closeDialog();
						getList();
					}
				}
			}
		};
		submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fNew&name=" + fName + "&type=Folder&where=" + currentFolder, true);
	} else {
		//New Files... not supported
	}
}

function Rename(fName, fNewName, fType) {
	var onreadyF = function() { 
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var resp = xmlhttp.responseText;
			if (window.DOMParser)
			{
				parser=new DOMParser();
				xmlDoc=parser.parseFromString(resp,"text/xml");
				var xmlElems = xmlDoc.getElementsByTagName("message");
				if(xmlElems.length != 0) {
					document.getElementById("error-action").innerHTML = xmlElems[0].childNodes[0].data;
				} else { //success
					closeDialog();
					getList();
				}
			}
		}
	};
	switch(fType) {
		case 'Folder' :
			submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fRename&name=" + fName + "&newname=" + fNewName + "&type=Folder&where=" + currentFolder, true);
			break;
		default :
			submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fRename&name=" + fName + "&newname=" + fNewName + "&type=file&where=" + currentFolder, true);
			break;
	}	
}

function Delete(fName, fType) {
	var onreadyF = function() { 
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var resp = xmlhttp.responseText;
			if (window.DOMParser)
			{
				parser=new DOMParser();
				xmlDoc=parser.parseFromString(resp,"text/xml");
				var xmlElems = xmlDoc.getElementsByTagName("message");
				if(xmlElems.length == 0) {
					//success
					getList();
				}
			}
		}
	};
	submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fRemove&name=" + fName + "&type=" + fType + "&where=" + currentFolder, true);
}

function Share(fName, uName, fType) {
	var onreadyF = function() { 
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var resp = xmlhttp.responseText;
			if (window.DOMParser)
			{
				parser=new DOMParser();
				xmlDoc=parser.parseFromString(resp,"text/xml");
				var xmlElems = xmlDoc.getElementsByTagName("message");
				if(xmlElems.length != 0) {
					document.getElementById("error-action").innerHTML = xmlElems[0].childNodes[0].data;
				} else { //success
					closeDialog();
					getList();
				}
			}
		}
	};
	submitRequest(onreadyF, null, "GET", "php/getff.php?mode=file&action=fShare&name=" + fName + "&type=" + fType + "&where=" + currentFolder + "&sharer=" + uName, true);
}