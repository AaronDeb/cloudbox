function $(elem)
{
	return document.getElementById(elem);
}


function submitRequest(onreadyFunc, onprogressFunc, requestVar, fullRequest, showLoadProgress)
{
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();	
		
		if(showLoadProgress)
		{
			var progressF = function(e)	{
				if(onprogressFunc != null)
				{
					onprogressFunc(e);
				}
				
				var pc = Math.round(e.loaded * 100 / e.total);
				if(pc == 100)
				{
					$('load').style.width = "0%";
				} else {
					$('load').style.width =  pc + "%";
				}
			}
			xmlhttp.onprogress = progressF;
		} else if(onprogressFunc != null)
		{
			xmlhttp.onprogress = onprogressFunc;
		}
		
		xmlhttp.onreadystatechange = onreadyFunc;
		xmlhttp.open(requestVar, fullRequest, false);
		xmlhttp.send();
	}
}