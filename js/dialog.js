function closeDialog()
{
	//destroy obj
	var elemid = document.getElementById("show-content");
	elemid.innerHTML = "";

	clearMenu();
	var objArray = [ ["New", function() {  nwe(); }] ];
	makeMenu(objArray);
	
	document.getElementById("opac-bck").style.display = 'none';
	document.getElementById("elem-dialog").style.display = 'none';
}
function showDialog(elem)
{
	G();
	document.getElementById("opac-bck").style.display = 'inline-block';
	var picdialog = document.getElementById("elem-dialog");
	picdialog.style.display = 'inline-block';
	
	//change element
	var imgid = document.getElementById("show-content");
	imgid.innerHTML = "";
	imgid.appendChild(elem);
	
	//position dialog
	var w = document.body.clientWidth;
	var bgPos = (parseInt(w, 10) /2) - ( ((parseInt(imgid.clientWidth, 10)) + 70) /2)
	picdialog.style.left = bgPos + 'px';
	
	var scrollpos = getScrollingPosition();
	picdialog.style.top = ((parseInt(document.body.clientHeight, 10) /2 - (parseInt(imgid.clientHeight, 10) /2)) + scrollpos[1]) + 'px';
}

function G()
{
	window.setInterval(function() {
		document.getElementById('opac-bck').style.height = document.body.scrollHeight + 'px';
	}, 1);
}
	
function getScrollingPosition()      
{      
	var position = [0, 0];      
	if (typeof window.pageYOffset != 'undefined')      
	{      
		position = [      
				window.pageXOffset,      
				window.pageYOffset      
			];      
	} else if (typeof document.documentElement.scrollTop != 'undefined' && document.documentElement.scrollTop > 0) {
		position = [      
				document.documentElement.scrollLeft,      
				document.documentElement.scrollTop      
			];      

	} else if (typeof document.body.scrollTop != 'undefined') {      
		position = [      
			document.body.scrollLeft,      
			document.body.scrollTop      
		];      

	}      
	return position;      

}