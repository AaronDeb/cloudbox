
authPage.controller('AuthController', function($scope, AuthService) {

	$scope.login = function(user) {

		$scope.formStatus = '';

		if ( ! $scope.loginShown ) {
			return;
		}

		if( user.name == undefined || user.password == undefined ){
			$scope.formStatus = "Error: Please fill in all the fields!";
			return;
		}

		AuthService.runLogin(user).then( function(data) {

			if(data["condition"] == "success")
			{
				$scope.formStatus = "Success!";
				$(".form-error")[0].style.color = 'green';
				location.href= data["message"] + '/control-panel/';
			}
			else if(data["condition"] == "failure")
			{
				$scope.formStatus = "Error: " + data["message"];
				$(".form-error")[0].style.color = 'red';
			}

		});
	};

	$scope.register = function(newuser) {

		$scope.formStatus = '';

		if ( $scope.loginShown ) {
			return;
		}

		if( newuser.uname == undefined || newuser.name == undefined || newuser.password == undefined || newuser.mail == undefined || newuser.key == undefined ){
			$scope.formStatus = "Error: Please fill in all the fields!";
			return;
		}

		AuthService.runRegister(newuser).then( function(data) {

			if(data["type"] == "success")
			{
				$(".form-error")[0].style.color = 'green';
				$scope.formStatus = "Success!";
			}
			else if(data["type"] == "failure")
			{
				$(".form-error")[0].style.color = 'red';
				$scope.formStatus = "Error: " + data["message"];
			}

		});
	};

	$scope.showKeyGen = function() {

		if( ! angular.element('#modal-overlay').hasClass('active') && !angular.element('#modal-overlay').hasClass('keyGenerated') ) {
			angular.element('#modal-overlay').addClass('active');
		}

	};

});


authPage.controller("KeygenController", function ($scope, $document) {

	$scope.textInpt=0;
	$scope.myKey= new Array();
	$scope.keyIndex= 0;
	$scope.roundInd= 0;

	$scope.incPgbar= function(percent) {
		pgElemp = document.getElementById("positive-pgbar");
		pgElemn = document.getElementById("negative-pgbar");
		var currWidth = parseInt(pgElemp.style.width, 10);
		var nextWidth = currWidth + percent;
		pgElemp.style.width = nextWidth + '%';
		pgElemn.style.width = (100 - nextWidth) + '%';
	};

	$scope.mouseMoveHandler= function( ) {
		var mouseX = 0;
		var mouseY = 0;

		mouseX = event.clientX + document.body.scrollLeft;
		mouseY = event.clientY + document.body.scrollTop;

		var totalXY = mouseX + mouseY;
		var hexKey = totalXY.toString(16);
		//document.getElementById("cursor-pos").innerHTML = '0x' + hexKey;

		if(hexKey != null)
		{
			if(this.myKey.length != 0)
			{
				if((hexKey.charAt(0) + hexKey.charAt(1)) != this.myKey[this.keyIndex-1])
				{
					this.myKey[this.keyIndex] = hexKey.charAt(0) + hexKey.charAt(1);
					var currentTextInpt = 'key' + this.textInpt;
					document.getElementById(currentTextInpt).innerText = this.myKey[this.keyIndex];

					//increase pg
					this.incPgbar(1);

					if(this.roundInd == 20)
					{
						//change color of this.textInpt minus
						if(this.textInpt <= 3)
						{
							elem = document.getElementById("minus-" + this.textInpt);
							elem.style.color = "#2793e6";

							if(this.textInpt == 3)
							{
								angular.element("#modal-overlay").off('mousemove');
								document.getElementById("set-key").disabled = false;
								document.getElementById("gen-desc").innerHTML = 'Key Generated!';
								document.getElementById("keygen-butt").onclick = null;
							}

						}
					}

					//increase indexes
					this.textInpt++;
					this.keyIndex++;
				}
			}
			else
			{
				if((hexKey.charAt(0) + hexKey.charAt(1)) != this.myKey[this.keyIndex-1])
				{
					this.myKey[this.keyIndex] = hexKey.charAt(0) + hexKey.charAt(1);
					var currentTextInpt = 'key' + this.textInpt;
					document.getElementById(currentTextInpt).value = this.myKey[this.keyIndex];

					//increase indexes
					this.textInpt++;
					this.keyIndex++;

					//increase pg
					this.incPgbar(1);
				}
			}

			if(this.textInpt == 5)
			{
				this.textInpt = 0;
				this.roundInd++;
			}
		}
	};
	
	$scope.clearBench = function () {
		//close ->>

		//keygen-dialog
		angular.element(".modal-dialog")[0].style.display = 'none';

		//opac-bck
		angular.element("#modal-overlay").removeClass('active');
		angular.element("#modal-overlay").addClass('keyGenerated');
	}

});


fileManagerPage.controller('FileBrowserController', function ($scope, $document, FolderTraversingService) {

	$scope.getRootFolder = function() {
		FolderTraversingService.getFolder(0).then( function(data) {
			var data = JSON.parse(data['data']);
			$scope.objlist =[];

			if(data.folders)
				$scope.objlist.push(data.folders);
			
			if(data.files)
				$scope.objlist.push(data.files);
		});
	}

});


fileManagerPage.controller('FileUploadController', function($scope) {

	$scope.toggleUploader = function() {
		elem = angular.element("#doclistmole")[0];

		if(elem.style.bottom == '-315px')
		{
			var bottom = -315;
			//Push it up
			function frameUp() {
				bottom += 3;  // update parameters
				elem.style.bottom = bottom + 'px'; // show frame
				if (bottom == 0)  // check finish condition
					clearInterval(id);
			}

			var id = setInterval(frameUp,  0.1); // draw every 10ms
		}
		else if(elem.style.bottom == '0px')
		{
			var bottom = 0;
			//push it down
			function frameDown() {
				bottom += -3;  // update parameters
				elem.style.bottom = bottom + 'px'; // show frame
				if (bottom == -315)  // check finish condition
					clearInterval(id);
			}

			var id = setInterval(frameDown,   0.1); // draw every 10ms
		}

	};

	$scope.openFileDialog = function() {
		angular.element('#fileselect').click();
	};

	// file drag hover
	$scope.FileDragHover = function(e) {
		e.stopPropagation();
		e.preventDefault();
		//e.target.className = (e.type == "dragover" ? "hover" : "");// ? "hover" : ""
	}

	// file selection
	$scope.FileSelectHandler = function(e) {
		// cancel event and hover styling
		$scope.FileDragHover(e);

		if(e.dataTransfer != null ||  e.target.files != null)
		{
			elem = document.getElementById("doclistmole");
			if(elem.style.bottom == '-315px')
			{
				toggleUploader();
			}

			// fetch FileList object
			var files = e.target.files || e.dataTransfer.files;
			// process all File objects
			for (var i = 0; i <= (files.length-1); i++) {
				$scope.addFile(files[i]);
			}
		}

	}

	$scope.httpreqArr = new Array();
	$scope.globalR =0;

	$scope.addFile = function(file)
	{
		var upelem = '#upload-file-';
		// create ROW
		var r;
		for(var i=0; i<=angular.element('#file-upload-list')[0].getElementsByTagName("tr").length; i++)
		{
			if(angular.element(upelem+i) == null)
			{
				r = i;
				angular.element('#file-upload-list').innerHTML = "<tr class='upload-file' onclick=\"if(document.getElementById('upload-state-" + r + "').innerHTML == 'Uploading..') { this.style['background-color'] = 'aliceblue'; document.getElementById('cancel-upload').style['display'] = 'inline-block'; }\" style='background-color: #FFF; border-bottom: 1px solid #ddd;line-height: 30px;white-space: nowrap;'><td id='upload-file-" + r + "' class='upload-file-name' style='padding-right: 3px;padding-left: 8px; width: 130px;' ></td><td><div id='pg-container' style='width:100%; height:15px; padding:2px 2px 2px 2px;'><div class='sparkbar-container' style='margin: 4px 0;height: 10px;overflow: hidden;'><div id='pos-" + r + "' class='positive-progress-bar' style='width: 0%;float: left;height: 10px;background: #2793e6;'></div><div id='neg-" + r + "' class='negative-progress-bar' style='width: 100%;float: left;height: 10px;background: #ccc;'></div></div></div></td><td><div id='percentage-progress-" + r + "'><center>0%</center></div></td><td id='upload-state-" + r + "' class='upload-file-action' style='font-size: 12px;width: 110px;padding-right: 8px;'>Uploading..</td></tr>" + angular.element('#file-upload-list').innerHTML;
				break;
			}
		}
		// output file information
		var m = angular.element("#upload-file-" + r);
		var fileNew = file.name.split('');
		var newFileName ="";
		if (fileNew.length >= 17)
		{
			for (var i =0; i<=17; i++) {
				newFileName += fileNew[i];
			}
			m.innerHTML = newFileName + "..";
		} else {
			m.innerHTML = file.name;
		}
		$scope.globalR = r;

		$scope.httpreqArr[r] = new XMLHttpRequest();
		var xhr = $scope.httpreqArr[r];

		if (xhr.upload && file.size <= 15728640) {

			// progress bar
			xhr.upload.onprogress = function(e) {
				if(e.lengthComputable)
				{
					/*var pc = Math.round(e.loaded * 100 / e.total);
					angular.element('#pos-' + r).style.width = pc + "%";
					angular.element('#neg-' + r).style.width = (100 - pc) + "%";
					angular.element('#percentage-progress-' + r).innerHTML = "<center>" + pc + "%</center>";*/
				}
			};

			// file received/failed
			xhr.onreadystatechange = function(e) {
				if (xhr.readyState == 4) {
					//progress.className = (xhr.status == 200 ? "success" : "failure");
					var xmlt = xhr.responseText;

					if(xmlt.condition == "success")
					{
						angular.element('#upload-state-' + r).innerHTML = "Uploaded";
					}
					else if(xmlt.condition == "failure")
					{
						angular.element('#upload-state-' + r).innerHTML = xmlt.message;
						angular.element('#pos-' + r).style.backgroundColor = '#E62C27';
					}
					//getList();
				}
			};


			xhr.open("POST", window.location.origin + "/cloudbox/api/file/new/", true);
			var fd = new FormData();
			fd.append("file_data", file);
			fd.append("where", 0);
			xhr.send(fd);
		} else {
			angular.element('#upload-state-' + r).innerHTML = "File Too Big!";
			angular.element('#neg-' + r).style.backgroundColor = '#E62C27';
		}
	}

	$scope.abortFile = function()
	{
		var clsArray = document.getElementsByClassName("upload-file");
		for(var i=0; i<=(clsArray.length-1); i++)
		{
			if(clsArray[i].style['background-color'] == 'rgb(240, 248, 255)')
			{
				clsArray[i].style["background-color"] = '#FFF';
				nm = clsArray[i].cells[0].id;
				nm = nm.replace("upload-file-", "");
				httpreqArr[nm].abort();
				angular.element('#cancel-upload').style.display = 'none';
				angular.element('#upload-state-' + nm).innerHTML = "Cancelled";
			}
		}

	}


	// initialize
	$scope.init = function() {

		var fileselect = angular.element("body")[0],
			filedrag = angular.element("body")[0];

		// file select
		fileselect.addEventListener("change", $scope.FileSelectHandler, false);

		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {
			// file drop
			filedrag.addEventListener("dragover", $scope.FileDragHover, false);
			filedrag.addEventListener("dragleave", $scope.FileDragHover, false);
			filedrag.addEventListener("drop", $scope.FileSelectHandler, false);
		}

	}

})