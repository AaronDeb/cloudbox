

authPage.directive('keygenDirective', function() {
    return {
        restrict: 'AE',
        template: '<div class="modal-dialog-content" ng-init="keyMade=false">'
        + '<label>Please move your mouse to increase your<br /></label>'
        + '<label>key\'s entropy(uniqueness):</label>'
        + '<br /><br />'
        + '<div style="display: inline-block;">'
        + '<div class="dialog-keyinput disabled" type="text" id="key0"></div>'
        + '<div class="minus" id="minus-0" style="color:#ccc;">-</div>'
        + '<div class="dialog-keyinput disabled" type="text" id="key1"></div>'
        + '<div class="minus" id="minus-1" style="color:#ccc;">-</div>'
        + '<div class="dialog-keyinput disabled" type="text" id="key2"></div>'
        + '<div class="minus" id="minus-2" style="color:#ccc;">-</div>'
        + '<div class="dialog-keyinput disabled" type="text" id="key3"></div>'
        + '<div class="minus" id="minus-3" style="color:#ccc;">-</div>'
        + '<div class="dialog-keyinput disabled" type="text" id="key4"></div>'
        + '</div>'
        + '</div>'
        + '<div class="modal-dialog-buttons">'
        + '<div id="pg-container" style="width:100%; height:15px; padding: 2px 0px 13px;">'
        + '<div class="sparkbar-container" style="margin: 4px 0;height: 10px;overflow: hidden;">'
        + '<div id="positive-pgbar" class="positive-progress-bar" style="width: 0%;float: left;height: 10px;background: #2793e6;"></div>'
        + '<div id="negative-pgbar" class="negative-progress-bar" style="width: 100%;float: left;height: 10px;background: #ccc;"></div>'
        + '</div>'
        + '</div>'
        + '<button id="set-key" name="ok" ng-click="clearBench();" disabled>OK</button>'
        + '</div>',
        controller: function ($scope, $element, $attrs, $transclude) {
            // controller logic goes here
        }
    }
});
