

authPage.factory('AuthService', function($http) {
    return {

      runLogin: function( user ) {

        return $http.get(window.location.origin + "/cloudbox/api/user/login/?username=" + user.name +"&password=" + user.password).then( function(result) {
            return result.data;
        });

      },

      runRegister: function (newuser) {

        return $http.get(window.location.origin + "/cloudbox/api/user/register/?username=" + newuser.uname + "&name=" + newuser.name + "&password=" + newuser.password + "&email=" + newuser.mail  +"&key=" + newuser.key).then( function(result) {
            return result.data;
        });

       }

    };

});



fileManagerPage.factory('FolderTraversingService', function($http) {
    return {

        getFolder: function( id ) {

            return $http.get(window.location.origin + "/cloudbox/api/folder/get/?folderParentId=" + id).then( function(result) {
                return result.data;
            });

        },

    };
});