function checkName()
{
	elem = document.getElementById("name");
	if(elem.value != "" || elem.value.length != 0)
	{
		if(hasWhiteSpace(elem.value))
		{
			elem.style.borderLeftColor = "#E62A25";//red
		}
		else
		{
			elem.style.borderLeftColor = "#2CE637"; //green
			if(elem.value.length > 0)
			{
				elem.style.borderLeftColor = "#2CE637"; //green
			}
			else
			{
				elem.style.borderLeftColor = "#E62A25";
			}
		}
	}
	else
	{
		elem.style.borderLeftColor = "#E62A25";
	}
}

function hasWhiteSpace(s) 
{
  return s.indexOf(' ') >= 0;
}

function checkUser()
{
}

function checkPass()
{
	var elem = document.getElementById("pass");
	if(elem.value != "" || elem.value.length != 0)
	{
		if (checkChars(elem.value))
		{
			elem.style.borderLeftColor = "#E62A25";//red
		}
		else
		{
			elem.style.borderLeftColor = "#2CE637"; //green
		}
	}
	else
	{
		elem.style.borderLeftColor = "#E62A25";
	}
}

function  checkChars(str)
{
	var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-="
	var check = function(string){
		for(i = 0; i < specialChars.length;i++){
			if(string.indexOf(specialChars[i]) > -1){
			   return true
			}
		}
		return false;
	}
	return check(str);
}

function checkMail()
{
	var elem = document.getElementById("mail");
	var atpos = elem.value.indexOf("@");
	var dotpos = elem.value.lastIndexOf(".");
	if(elem.value != "" || elem.value.length != 0)
	{
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=elem.value.length)
		{
			elem.style.borderLeftColor = "#E62A25";//red
		}
		else
		{
			elem.style.borderLeftColor = "#2CE637"; //green
		}
	}
	else
	{
		elem.style.borderLeftColor = "#E62A25";
	}
}