function $id(id) {
	return document.getElementById(id);
}

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	//e.target.className = (e.type == "dragover" ? "hover" : "");// ? "hover" : ""
}

// file selection
function FileSelectHandler(e) {
	// cancel event and hover styling
	FileDragHover(e);
	
	if(e.dataTransfer != null ||  e.target.files != null)
	{
		elem = document.getElementById("doclistmole");
		if(elem.style.bottom == '-315px')
		{
			openUploader();
		}
	
		// fetch FileList object
		var files = e.target.files || e.dataTransfer.files;
		// process all File objects
		for (var i = 0; i <= (files.length-1); i++) {
			addFile(files[i]);
		}
	}

}

var httpreqArr = new Array();
var globalR =0;

function addFile(file)
{
	var upelem = 'upload-file-';
	// create ROW
	var r;
	for(var i=0; i<=$id('file-upload-list').getElementsByTagName("tr").length; i++)
	{
		if($id(upelem+i) == null)
		{
			r = i;
			$id('file-upload-list').innerHTML = "<tr class='upload-file' onclick=\"if(document.getElementById('upload-state-" + r + "').innerHTML == 'Uploading..') { this.style['background-color'] = 'aliceblue'; document.getElementById('cancel-upload').style['display'] = 'inline-block'; }\" style='background-color: #FFF; border-bottom: 1px solid #ddd;line-height: 30px;white-space: nowrap;'><td id='upload-file-" + r + "' class='upload-file-name' style='padding-right: 3px;padding-left: 8px; width: 130px;' ></td><td><div id='pg-container' style='width:100%; height:15px; padding:2px 2px 2px 2px;'><div class='sparkbar-container' style='margin: 4px 0;height: 10px;overflow: hidden;'><div id='pos-" + r + "' class='positive-progress-bar' style='width: 0%;float: left;height: 10px;background: #2793e6;'></div><div id='neg-" + r + "' class='negative-progress-bar' style='width: 100%;float: left;height: 10px;background: #ccc;'></div></div></div></td><td><div id='percentage-progress-" + r + "'><center>0%</center></div></td><td id='upload-state-" + r + "' class='upload-file-action' style='font-size: 12px;width: 110px;padding-right: 8px;'>Uploading..</td></tr>" + $id('file-upload-list').innerHTML;
			break;
		}
	}
	// output file information
	var m = $id("upload-file-" + r);
	var fileNew = file.name.split('');
	var newFileName ="";
	if (fileNew.length >= 17)
	{
		for (var i =0; i<=17; i++) {
			newFileName += fileNew[i];
		}
		m.innerHTML = newFileName + "..";
	} else {
		m.innerHTML = file.name;
	}
	globalR = r;
	
	httpreqArr[r] = new XMLHttpRequest();
	var xhr = httpreqArr[r];
	
	if (xhr.upload && file.size <= 15728640) {
		
		// progress bar
		xhr.upload.onprogress = function(e) {
			if(e.lengthComputable)
			{
				var pc = Math.round(e.loaded * 100 / e.total);
				$id('pos-' + r).style.width = pc + "%";
				$id('neg-' + r).style.width = (100 - pc) + "%";
				$id('percentage-progress-' + r).innerHTML = "<center>" + pc + "%</center>";
			}
		};

		// file received/failed
		xhr.onreadystatechange = function(e) {
			if (xhr.readyState == 4) {
				//progress.className = (xhr.status == 200 ? "success" : "failure");
				var xmlt = xhr.responseText;
				if (window.DOMParser)
				{
					parser=new DOMParser();
					xmlDoc=parser.parseFromString(xmlt,"text/xml");
				}
				
				if(xmlDoc.getElementsByTagName("type")[0].childNodes[0].nodeValue == "success")
				{
					$id('upload-state-' + r).innerHTML = "Uploaded";
				}
				else if(xmlDoc.getElementsByTagName("type")[0].childNodes[0].nodeValue == "error")
				{
					$id('upload-state-' + r).innerHTML = xmlDoc.getElementsByTagName("message")[0].childNodes[0].nodeValue;
					$id('pos-' + r).style.backgroundColor = '#E62C27';
				}
				getList();
			}
		};
		

		xhr.open("POST", "php/upload.php", true);
		var fd = new FormData();
		fd.append("file_data", file);
		fd.append("where", currentFolder);
		xhr.send(fd);
	} else {
		$id('upload-state-' + r).innerHTML = "File Too Big!";
		$id('neg-' + r).style.backgroundColor = '#E62C27';		
	}
}

function abortFile()
{
	var clsArray = document.getElementsByClassName("upload-file");
	for(var i=0; i<=(clsArray.length-1); i++)
	{
		if(clsArray[i].style['background-color'] == 'rgb(240, 248, 255)')
		{
			clsArray[i].style["background-color"] = '#FFF'; 
			nm = clsArray[i].cells[0].id;
			nm = nm.replace("upload-file-", "");
			httpreqArr[nm].abort();
			$id('cancel-upload').style.display = 'none';
			$id('upload-state-' + nm).innerHTML = "Cancelled";
		}
	}

}


// initialize
function Init() {

	var fileselect = $id("file-drag"),
		filedrag = $id("file-drag");

	// file select
	fileselect.addEventListener("change", FileSelectHandler, false);

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if (xhr.upload) {
		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
	}

}