// objArray = [ ["Name", "OnClickStuff"] ];
function makeMenu(objArray) {
	var mode =1;
	var interactionMenu = document.getElementsByClassName('interaction-buttons')[0];
	//Clear Menu
	clearMenu();
	
	for(var x =0; x<=objArray.length-1; x++) {
		//change color mode
		if(mode == 0) {	mode = 1; }
		else { mode = 0; }
		
		//Fabricate Button Element
		var elemButton = makeElement(objArray[x][0], mode);
		elemButton.onclick = objArray[x][1];
		interactionMenu.appendChild(elemButton);
	}

}

function clearMenu() {
	var interactionMenu = document.getElementsByClassName('interaction-buttons')[0];
	//Clear Menu
	while (interactionMenu.firstChild) {
		interactionMenu.removeChild(interactionMenu.firstChild);
	}
}
	
/*
<div id="button-wut" style="float:left;">
	<a class="button color" id="button-size">
		<img src="images/wutW-G.png">
		<br />
		<span class="bdescr">Back</span>
	</a>
</div>
*/

function makeElement(name, mode) {
	var elemButton = document.createElement('div');
	elemButton.style['float'] = 'left';
	
	var elemLink = document.createElement('a');
	elemLink.id = 'button-size';
	
	
	var elemIMG  = document.createElement('img');
	if(mode == 0) {	
		elemLink.className = 'button color';
		elemIMG.src = 'images/icons/w/' + name + '.png'; 
	} else {	
		elemLink.className = 'button secondary';
		elemIMG.src = 'images/icons/g/' + name + '.png'; 
	}
	
	var elemDesc = document.createElement('span');
	elemDesc.className = 'bdescr';
	elemDesc.textContent = name;
	
	elemLink.appendChild(elemIMG);
	elemLink.innerHTML = elemLink.innerHTML + '<br />';
	elemLink.appendChild(elemDesc);
	
	elemButton.appendChild(elemLink);
	return elemButton;
}