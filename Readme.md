#Advanced File Manager v.2.0.0

##Changelog:
    1. Features
        1. Shave 2K loc
        2. More Comments
        3. Make a micro error reporting framework to report events to admin
        4. Make landing page
        5. Will try to remove redundant features like the encryption
        vault etc..
        6. Will add more supporting file editor/viewer (s). Presently
        video/audio/folders can be opened. In the future documents/
        spreadsheets and more protocols will be implemented to be 
        read/written.
    
    2. Frontend
        1. Recoded most of the templates with simplicity in mind
        (removing clutter / div soups and removing as much inline 
        css code as possible)
        2. Using more bleeding edge technologies to facilitate
        template work like angular and in
        some cases jquery and underscore.js
        3. The animation on the login page will be remade using
        a better framework for animation.
        4. The keygen system will be reworked.
        5. The file browsing page will be structurally remade
        to support more features.
        6. Make a better admin area.
        
    3. Backend
        1. Add a custom router and templating engine framework
        that will manage the rendering and processing of requests
        to the server.
        2. Maybe add caching to the template engine?
        3. Change the database system from MySQL to MongoDB
        4. The files will be stored in Mongo's HDFS and the
        other data will be serialized into json and stored
        into the document collection system.
        5. Start using a REST framework system with JSON
        serialized return data.
        6. Get better server analytics ?maybe?
    