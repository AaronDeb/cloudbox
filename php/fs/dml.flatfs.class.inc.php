<?PHP
include "ifs.php";

class FfsDAO implements IFS{ //FLAT FILE SYSTEM

    public function readFile( $name ){
        return file_get_contents($name);
    }

    public function writeFile( $name, $contents){
        file_put_contents( $name, $contents);
    }

    public function downloadFile( $name ) {

    }

    public function deleteFile( $name ){
        unlink( $name );
    }

}

?>
