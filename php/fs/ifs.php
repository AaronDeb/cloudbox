<?php

interface IFS {
    public function readFile( $name );
    public function writeFile( $name, $contents );
    public function downloadFile( $name );
    public function deleteFile( $name );
}

?>