<?PHP
include "idb.php";

/*
 *      @classinfo : Db model controller which saves and retrieves unserialized models from the db 
 *
 */

class MySqlDAO implements IDB{
    private $dbHandler;
    private $dbName;

    public function __construct($args=array("localhost", "root", "", "cloudbox"))
    {
        $db = new mysqli($args[0], $args[1], $args[2], $args[3]);

        if($db->connect_errno > 0){
            die('Unable to connect to database [' . $db->connect_error . ']');
        }

        $this->dbHandler = $db;
        $this->dbName = $args[3];
    }

    public function getData($tableName, $where) {
        $stringifiedWhere = '';
        $index =0;
        //stringify $where
        foreach( $where as $key => $value ) {
            if( is_string($value) ) {
                $value = "'" . $this->dbHandler->escape_string($value) . "'";
            }

            if ( $index > 0 ) {
                $stringifiedWhere .= "AND `$key` = $value ";
            } else {
                $stringifiedWhere .= "`$key` = $value ";
            }
            $index++;
        }

        $sql = $this->dbHandler->prepare("SELECT * FROM `" . $tableName . "` WHERE " . $stringifiedWhere);

        if( $result = $sql->execute() ) {

            $meta = $sql->result_metadata();

            while ($field = $meta->fetch_field()) {
                $params[] = &$row[$field->name];
            }

            call_user_func_array(array($sql, 'bind_result'), $params);
            while ($sql->fetch()) {
                foreach($row as $key => $val) {
                    $c[$key] = $val;
                }
                $hits[] = $c;
            }
            $sql->close();

            if( isset($hits) ) {
                return $hits;
            } else {
                return [];
            }

        } else {
            $sql->close();
            die('There was an error running the query [' . $this->dbHandler->error . ']');
        }
    }

    public function insertRecord($tableName, $fieldArray) {
        $stringifiedColumnArray = '';
        $stringifiedValueArray = '';

        $index =0;
        $lastCol = count($fieldArray)-1;
        //stringify $fieldArray
        foreach( $fieldArray as $key => $value ) {
            $value = "'" . $this->dbHandler->escape_string($value) . "'";

            if ( $lastCol == $index ) { //check if last value
                $stringifiedColumnArray .= $key;
                $stringifiedValueArray .= $value;
            } else {
                $stringifiedColumnArray .= $key . ",";
                $stringifiedValueArray .= $value . ",";
            }

            $index++;
        }

        $sql = $this->dbHandler->prepare("INSERT INTO `" . $tableName . "` (" . $stringifiedColumnArray . ") VALUES (" . $stringifiedValueArray . ")");

        if(!$result = $sql->execute()){
            $sql->close();
            die('There was an error running the query [' . $this->dbHandler->error . ']');
        } else {
            $sql->close();
            return true;
        }
    }

    public function updateRecord($tableName, $where, $newArray) {
        $stringifiedNewArray = '';
        $stringifiedWhere = '';

        $index =0;
        $lastCol = count($newArray)-1;
        //stringify $newArray
        foreach( $newArray as $key => $value ) {
            $value = "'" . $this->dbHandler->escape_string($value) . "'";

            if ( $index > 1 ) {
                $stringifiedNewArray .= ", `$key` = $value";
            } else {
                $stringifiedNewArray .= "`$key` = $value ";
            }

            $index++;
        }

        //stringify $oldArray
        foreach( $where as $key => $value ) {
            $value = "'" . $this->dbHandler->escape_string($value) . "'";

            if ( $index > 1 ) {
                $stringifiedWhere .= "AND `$key` = $value";
            } else {
                $stringifiedWhere .= "`$key` = $value ";
            }

            $index++;
        }

        $sql = $this->dbHandler->prepare("UPDATE `" . $tableName . "` SET ". $stringifiedNewArray . " WHERE ". $stringifiedWhere);

        if(!$result = $sql->execute()){
            $sql->close();
            die('There was an error running the query [' . $this->dbHandler->error . ']');
        }else {
            $sql->close();
            return true;
        }
    }

    public function deleteRecord($tableName, $fieldArray) {
        $stringifiedFieldArray = '';
        $index =0;

        //stringify $fieldArray
        foreach( $fieldArray as $key => $value ) {
            if( is_string($value) ) {
                $value = $this->dbHandler->escape_string($value);
            }

            if ( $index > 1 ) {
                $stringifiedFieldArray .= "AND `$key` = $value";
            } else {
                $stringifiedFieldArray .= "`$key` = $value ";
            }

            $index++;
        }

        $sql = $this->dbHandler->prepare("DELETE FROM `" . $tableName . "` WHERE " . $stringifiedFieldArray );

        if(!$result = $sql->execute()){
            $sql->close();
            die('There was an error running the query [' . $this->dbHandler->error . ']');
        }else {
            $sql->close();
            return true;
        }
    }

}

?>
