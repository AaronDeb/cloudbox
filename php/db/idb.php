<?php

interface IDB {
    public function getData($tableName, $where);
    public function insertRecord($tableName, $fieldArray);
    public function updateRecord($tableName, $where, $newArray);
    public function deleteRecord($tableName, $fieldArray);
}

?>