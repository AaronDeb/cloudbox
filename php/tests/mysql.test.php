<?php

include "../db/dml.mysql.class.inc.php";


function insertRecord_test() {
    $dao = new MySqlDAO();
    assert( $dao->insertRecord("tblfiles", ['clmnFileID' => 123, 'clmnUNFile' => 'admin']) );
}

function getData_test() {
    $dao = new MySqlDAO();
    $ret = $dao->getData("tblfiles", ['clmnFileID' => 123, 'clmnUNFile' => 'admin']);
    assert( sizeof($ret) >= 0 );
}

function updateRecord_test() {
    $dao = new MySqlDAO();
    assert( $dao->updateRecord( "tblfiles", ['clmnFileID' => 123], ['clmnEncrypted' => '1'] ) );
}

function deleteRecord_test() {
    $dao = new MySqlDAO();
    assert( $dao->deleteRecord( "tblfiles", ['clmnFileID' => 123] ) );
}


insertRecord_test(); //passed
getData_test(); //passed
updateRecord_test(); //passed
getData_test(); //passed
deleteRecord_test(); //passed
getData_test(); //passed

?>