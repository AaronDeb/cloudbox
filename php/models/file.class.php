<?php

include "/../interfaces/ifile.php";

class File implements IFile {
    /* MODEL STUFF */
    private $fileId;
    private $fileName;
    private $fileOwner;
    private $fileSize;
    private $fileModified;
    private $fileMimeType;
    private $fileMappedName;
    private $fileEncrypted;
    private $fileParentFolder;

    public function __construct($fileId, $fileName, $fileOwner, $fileSize, $fileModified, $fileMimeType, $fileMappedName, $fileEncrypted, $parentFolderID ) {
        $this->fileId = $fileId;
        $this->fileName = $fileName;
        $this->fileOwner = $fileOwner;
        $this->fileSize = $fileSize;
        $this->fileModified = $fileModified;
        $this->fileMimeType = $fileMimeType;
        $this->fileMappedName = $fileMappedName;
        $this->fileEncrypted = $fileEncrypted;
        $this->fileParentFolder = $parentFolderID;
    }

    /* GETTERS */
    public function getFileId() {
        return $this->fileId;
    }

    public function getFileName() {
        return $this->fileName;
    }

    public function getFileOwner() {
        return $this->fileOwner;
    }

    public function getFileSize() {
        return $this->fileSize;
    }

    public function getFileModified() {
        return $this->fileModified;
    }

    public function getFileMimeType() {
        return $this->fileMimeType;
    }

    public function getFileMappedName() {
        return $this->fileMappedName;
    }

    public function getFileEncrypted() {
        return $this->fileEncrypted;
    }

    public function getFileParentFolder() {
        return $this->fileParentFolder;
    }

    /* SETTERS */
    public function setFileName($name) {
        if( strlen( $name ) < 40 ) { //limit to 40 chars
            $this->fileName = $name;
        } else {
            die("The filename is too long!");
        }
    }

    public function setFileOwner($owner) {
        $this->fileOwner = $owner;
    }

    public function setFileSize($size) {
        $this->fileSize = $size;
    }

    public function setFileModified($modified) {
        $this->fileModified = $modified;
    }

    public function setFileMimeType($mimeType) {
        $this->fileMimeType = $mimeType;
    }

    public function setFileMappedName($mappedName) {
        $this->fileMappedName = $mappedName;
    }

    public function setFileEncrypted($encrypted) {
        $this->fileEncrypted = $encrypted;
    }

    public function setFileParentFolder($id) {
        $this->fileParentFolder = $id;
    }
    
}


?>