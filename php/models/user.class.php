<?php

include "/../interfaces/iuser.php";

class User implements IUser{
	/* MODEL STUFF */
	private $id;
	private $username;
	private $name;
	private $passw;
	private $email;
	private $key;
	private $isAdmin;

	public function __construct($id, $username, $name, $passw, $isAdmin, $email, $key) {
		$this->id = $id;
		$this->username = $username;
		$this->name = $name;
		$this->passw = $passw;
		$this->isAdmin = $isAdmin;
		$this->email = $email;
		$this->key = $key;

	}

	/* GETTERS */

	public function getID() {
		return $this->id;
	}

	public function getUserName() {
		return $this->username;
	}

	public function getName() {
		return $this->name;
	}
	
	public function getPassword() {
		return $this->passw;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getKey() {
		return $this->key;
	}

	public function getIsAdmin() {
		return $this->isAdmin;
	}

	/* SETTERS */

	public function setName($name) {
		if( empty($name) || sizeof( preg_match('/\s/', $name ) == 0 ) ) { //check if empty and it contains whitespace
			die("Name is invalid!");
		}
		$this->name = $name;
	}
		
	public function setEmail($email) {
		if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			die("Email is not valid!");
		}
		$this->email = $email;
	}
	
	public function setPassword($passw) {
		if( !(strlen( $passw ) >= 7 )) {
			die("Password is too short!");
		}
		$this->passw = $passw;
	}
	
	public function setKey($key) {
		if( empty($key) || sizeof( $key ) != 9 ) { //check if empty and it contains whitespace
			die("Key is invalid!");
		}
		$this->key = $key;
	}

	public function setIsAdmin($isAdmin) {
		$this->isAdmin = $isAdmin;
	}

	public function toArray() {
		return array(
			'name' => self::$name,
			'email' => self::$email,
			'passw' => self::$passw,
			'isAdmin' => self::$isAdmin,
			'key' => self::$key 	);
	}
}

?>
