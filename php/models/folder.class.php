<?php

include "/../interfaces/ifolder.php";

class Folder implements IFolder{
    /* MODEL STUFF */
    private $folderId;
    private $name;
    private $folderOwnerId;
    private $userId;

    public function __construct($folderId, $name, $folderInId, $userId) {
        $this->folderId = $folderId;
        $this->name = $name;
        $this->folderOwnerId = $folderInId;
        $this->userId = $userId;
    }

    /* GETTERS */

    public function getFolderId() {
        return $this->folderId;
    }

    public function getFolderName() {
        return $this->name;
    }

    public function getFolderOwnerId() {
        return $this->folderOwnerId;
    }

    public function getOwnerID() {
        return $this->userId;
    }


    /* SETTERS */

    public function setFolderId($id) {
        $this->folderId = $id;
    }

    public function setFolderName($name) {
        $this->name = $name;
    }

    public function setFolderOwnerId($id) {
        $this->folderOwnerId = $id;
    }

    public function setOwnerID($id) {
        $this->userId = $id;
    }
}

?>
