<?PHP
include 'init.php';
session_start();

$mode = $_GET['mode'];

echo "<flrequest>";

switch($mode){
	case "file" :
		$action = $_GET['action'];

		switch ($action) {
			case "fOpen" :
				$fileName = $_GET['filename'];
				$type = $_GET['type'];
				$folder = $_GET['folder'];

				if($folder == "") {
					$fTypeResult = dbUtil::SELECT("SELECT clmnFileType FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname AND clmnFoldIn='root'", array(':uname' => $_SESSION['un'], ':fname' => $fileName));
					if(count($fTypeResult) != 0) {
						$fType = $fTypeResult[0]['clmnFileType'];
					} 
				} else {
					$fTypeResult = dbUtil::SELECT("SELECT clmnFileType FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname AND clmnFoldIn=:foldName", array(':uname' => $_SESSION['un'], ':fname' => $fileName,':foldName' => $folder) );
					if(count($fTypeResult) != 0) {
						$fType = $fTypeResult[0]['clmnFileType'];
					} 
				}
				/*
				filename actual if type == actType
				*/
				$fileType =  explode("/", $fType);
				if($type == $fileType[0])
				{
					switch($type) {
						case "image" :
							echo "<filecontents>";
							$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $fileName));
							if(count($fActNameResult) != 0) {
								$actFN = $fActNameResult[0]['clmnActFN'];
							}
							$data = file_get_contents("../files/" . $actFN);
							echo base64_encode($data);
							echo "</filecontents>";
							break;
						case "video" :
							echo "<videofname>";
							$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $fileName));
							if(count($fActNameResult) != 0) {
								$actFN = $fActNameResult[0]['clmnActFN'];
							}
							$data = copy("../files/" . $actFN, "../temp/" . $fileName);
							echo "/temp/" . $fileName;
							echo "</videofname>";
							break;
						default :
							break; 
					}
				}
				break;
			case 'fNew' :
				$name = $_GET['name'];
				$type = $_GET['type'];
				$where = $_GET['where'];

				if($type == 'Folder') {
					//check if there is a folder named the same
					$fCheckResult = dbUtil::SELECT("SELECT clmnFoldID FROM tblFolders WHERE clmnUNFolder=:uname AND clmnFoldIn=:foldin AND clmnFoldName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $name, ':foldin' => $where));
					if(count($fCheckResult) == 0) {
						//if not add folder
						$exc = dbUtil::INSERT("INSERT INTO tblFolders VALUES('', :foldName, :foldIn, :UNfold)", array(':foldName' => $name, ':foldIn' => $where, ':UNfold' => $_SESSION['un']));
						if($exc == true)
						{
							//sucess
						}
					} else {
						//Throw error
						xml_encode("error", "Folder already exists!");
					}
					
				}
				break;		
			case 'fRename' :
				$fName = $_GET['name'];
				$newName = $_GET['newname'];
				$type = $_GET['type'];
				$where = $_GET['where'];

				if($type == 'Folder') {
					//check if there is a folder named the same
					$fCheckResult = dbUtil::SELECT("SELECT clmnFoldID FROM tblFolders WHERE clmnUNFolder=:uname AND clmnFoldIn=:foldin AND clmnFoldName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $newName, ':foldin' => $where));
					if(count($fCheckResult) == 0) {
						//if not update folder name
						$exc = dbUtil::UPDATE("UPDATE tblFolders SET clmnFoldName=:newFoldName WHERE clmnUNFolder=:uname AND clmnFoldIn=:foldin AND clmnFoldName=:fname", array(':newFoldName' => $newName, ':uname' => $_SESSION['un'], ':fname' => $fName, ':foldin' => $where));
						if($exc == true)
						{
							//sucess
						}
					} else {
						//Throw error
						xml_encode("error", "Folder already exists!");
					}
				} else {
					//check if there is a file named the same
					$fCheckResult = dbUtil::SELECT("SELECT clmnFileID FROM tblFiles WHERE clmnFileName=:fileName AND clmnFoldIn=:foldin AND clmnUNFile=:uname", array(':fileName' => $newName, ':uname' => $_SESSION['un'], ':foldin' => $where));
					if(count($fCheckResult) == 0) {
						//if not update file name
						$exc = dbUtil::UPDATE("UPDATE tblFiles SET clmnFileName=:newFileName WHERE clmnFileName=:fileName AND clmnFoldIn=:foldin AND clmnUNFile=:uname", array(':newFileName' => $newName, ':uname' => $_SESSION['un'], ':fileName' => $fName, ':foldin' => $where));
						if($exc == true)
						{
							//sucess
						}
					} else {
						//Throw error
						xml_encode("error", "File already exists!");
					}
				}
				break;
			case 'fRemove' :
				$fName = $_GET['name'];
				$type = $_GET['type'];
				$where = $_GET['where'];

				if($type == 'Folder') {
					$fCheckResult = dbUtil::SELECT("SELECT clmnFoldID FROM tblFolders WHERE clmnUNFolder=:uname AND clmnFoldIn=:foldin AND clmnFoldName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $fName, ':foldin' => $where));
					if(count($fCheckResult) != 0) {
						echo $fCheckResult[0]['clmnFoldID'];
						dbUtil::DELETE("DELETE FROM tblFolders WHERE clmnFoldID=:foldId", array(':foldId' => $fCheckResult[0]['clmnFoldID']));
					}
				} else {
					//Get fileid from selection of columns
					//DELETE row with that fileid
					//DELETE shared row with that fileid
					$fCheckResult = dbUtil::SELECT("SELECT clmnFileID FROM tblFiles WHERE clmnFileName=:fileName AND clmnFoldIn=:foldin AND clmnUNFile=:uname", array(':fileName' => $fName, ':uname' => $_SESSION['un'], ':foldin' => $where));
					if(count($fCheckResult) != 0) {
						dbUtil::DELETE("DELETE FROM tblFiles WHERE clmnFileID=:fileId", array(':fileId' => $fCheckResult[0]['clmnFileID']));
						dbUtil::DELETE("DELETE FROM tblSharedFiles WHERE clmnFileID=:fileId", array(':fileId' => $fCheckResult[0]['clmnFileID']));
					}
				}
				break;
			case 'fEncrypt' :
				$fName = $_GET['name'];
				$type = $_GET['type'];
				$where = $_GET['where'];
				
				//Set file as Encrypted
				$exc = dbUtil::UPDATE("UPDATE tblFiles SET clmnEncrypted='1' WHERE clmnFileName=:fileName AND clmnFoldIn=:foldin AND clmnUNFile=:uname", array(':uname' => $_SESSION['un'], ':fileName' => $fName, ':foldin' => $where));
				if($exc == true)
				{
					//sucess
				}
				//Encrypt File
				$kCheckResult = dbUtil::SELECT("SELECT clmnKey FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
				if(count($kCheckResult) != 0) {
					$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $fName));
					if(count($fActNameResult) != 0) {
						$actFN = $fActNameResult[0]['clmnActFN'];
					}
					$key = $kCheckResult[0]['clmnKey'];
					$f = fopen("../files/" . $actFN, "r+");
					$len = filesize("../files/" . $actFN);
					$input = fread($f, $len);

					$data = base64_encode($input);

					fwrite($f, $data);
					fclose($fw);
				}

				break;
			case 'fDecrypt' :
				$fName = $_GET['name'];
				$type = $_GET['type'];
				$where = $_GET['where'];
				
				//Set file as Encrypted
				$exc = dbUtil::UPDATE("UPDATE tblFiles SET clmnEncrypted='0' WHERE clmnFileName=:fileName AND clmnFoldIn=:foldin AND clmnUNFile=:uname", array(':uname' => $_SESSION['un'], ':fileName' => $fName, ':foldin' => $where));
				if($exc == true)
				{
					//sucess
				}
				//Encrypt File
				$kCheckResult = dbUtil::SELECT("SELECT clmnKey FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
				if(count($kCheckResult) != 0) {
					$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $fName));
					if(count($fActNameResult) != 0) {
						$actFN = $fActNameResult[0]['clmnActFN'];
					}
					$key = $kCheckResult[0]['clmnKey'];
					$f = fopen("../files/" . $actFN, "r+");
					$len = filesize("../files/" . $actFN);
					$input = fread($f, $len);

					$data = base64_decode($input);

					fwrite($f, $data);
					fclose($fw);
				}

				break;
			case 'fShare' :
				$fName = $_GET['name'];
				$toShare = $_GET['sharer'];
				$type = $_GET['type'];
				$where = $_GET['where'];
				$shareCheckResult = dbUtil::SELECT("SELECT clmnID FROM tblUsers WHERE clmnUserName=:usrname", array(':usrname' => $toShare));
				if(count($shareCheckResult) == 0) {
					xml_encode("error", "Sharer Not Found!");
					die();
				}

				$userIdResult = dbUtil::SELECT("SELECT clmnId FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
				if(count($userIdResult) != 0) {
					$userId = $userIdResult[0]['clmnId'];
				} 

				$fCheckResult = dbUtil::SELECT("SELECT clmnFileID FROM tblFiles WHERE clmnFileName=:fileName AND clmnFoldIn=:foldin AND clmnUNFile=:uname", array(':fileName' => $fName, ':uname' => $_SESSION['un'], ':foldin' => $where));
				if(count($fCheckResult) != 0) {
					$sCheckResult = dbUtil::SELECT("SELECT clmnShrId FROM tblSharedFiles WHERE clmnFileID=:fileId", array(':fileId' => $fCheckResult[0]['clmnFileID']));
					if(count($sCheckResult) == 0) {
						$exc = dbUtil::INSERT("INSERT INTO tblSharedFiles VALUES('', :fileid, :fileown, :unfold)", array(':fileid' => $fCheckResult[0]['clmnFileID'], ':unfold' => $shareCheckResult[0]['clmnID'], ':fileown' => $userId));
						if($exc == true)
						{
							//sucess
						}
					}
				}
				break;
			case 'fUnShare' :
				$fName = $_GET['name'];
				$type = $_GET['type'];
				$where = $_GET['where'];
				$fCheckResult = dbUtil::SELECT("SELECT clmnFileID FROM tblFiles WHERE clmnFileName=:fileName AND clmnFoldIn=:foldin AND clmnUNFile=:uname", array(':fileName' => $fName, ':uname' => $_SESSION['un'], ':foldin' => $where));
				if(count($fCheckResult) != 0) {
					dbUtil::DELETE("DELETE FROM tblSharedFiles WHERE clmnFileID=:fileId", array(':fileId' => $fCheckResult[0]['clmnFileID']));
				}
				break;
			case 'fDownload' :
				$dmode = $_GET['dmode'];
				switch ($dmode) {
					case 'single':
						$fName = $_GET['name'];
						$type = $_GET['type'];
						$where = $_GET['where'];

						if($type == 'Folder') {
							//recursive 
							if(!(is_dir("../temp/" . $fName)))
							{
								mkdir("../temp/" . $fName);
							}

							$fFolderFilesResult = dbUtil::SELECT("SELECT clmnFileID FROM tblFiles WHERE clmnUNFile=:uname AND clmnFoldIn=:foldin", array(':uname' => $_SESSION['un'], ':foldin' => $fName));
							if(count($fFolderFilesResult) != 0) {

								foreach($fFolderFilesResult as $row) {
									$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnFileID=:fileid", array(':fileid' => $row['clmnFileID']));
									if(count($fActNameResult) != 0) {
										$actFN = $fActNameResult[0]['clmnActFN'];
										copy("../files/" . $actFN, "../temp/" . $fName);
									}
								}

							}
						//Zip Folder
						Zip("../temp/" . $fName, "../temp/" . $fName, $fName);
						//Remove Directory
						} else {
							$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname", array(':uname' => $_SESSION['un'], ':fname' => $fName));
							if(count($fActNameResult) != 0) {
								$actFN = $fActNameResult[0]['clmnActFN'];
							}
							copy("../files/" . $actFN, "../temp/" . $fName);
						}

						break;
					case 'getkey':
						echo "<key>" . md5(generateRandomString()) . "</key>";
						break;
					case 'package':
						//move files or folders to respective <key> directory
						$fName = $_GET['name'];
						$type = $_GET['type'];
						$where = $_GET['where'];
						$key = $_GET['key'];

						if(!(is_dir("../temp/" . $key . "/")))
						{
							mkdir("../temp/" . $key . "/");
						}

						if($type == 'Folder') {
							//recursive .
							if(!(is_dir("../temp/" . $key . "/" . $fName)))
							{
								mkdir("../temp/" . $key . "/" . $fName);
							}

							$fFolderFilesResult = dbUtil::SELECT("SELECT clmnFileID, clmnFileName FROM tblFiles WHERE clmnUNFile=:uname AND clmnFoldIn=:foldin", array(':uname' => $_SESSION['un'], ':foldin' => $fName));
							if(count($fFolderFilesResult) != 0) {

								foreach($fFolderFilesResult as $row) {
									$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnFileID=:fileid", array(':fileid' => $row['clmnFileID']));
									if(count($fActNameResult) != 0) {
										$actFN = $fActNameResult[0]['clmnActFN'];
									}
									copy("../files/" . $actFN, "../temp/" . $key . "/" . $fName . "/". $row['clmnFileName']);
								}

							}
							
						} else {
							$fActNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnUNFile=:uname AND clmnFileName=:fname AND clmnFoldIn=:foldin", array(':uname' => $_SESSION['un'], ':fname' => $fName, ':foldin' => $where));
							if(count($fActNameResult) != 0) {
								$actFN = $fActNameResult[0]['clmnActFN'];
							}
							copy("../files/" . $actFN, "../temp/" . $key . "/" . $fName);
						}
						break;
					case 'compress':
						$key = $_GET['key'];

						//Zip Folder
						Zip("../temp/" . $key, "../temp/" . $key . "/", $key);
						
						$filename = $key . ".zip"; 
						$filepath = "temp/";

						//Remove file
						//unlink("../temp/" . $key . ".zip");

						//Remove Directory
						removeDir("../temp/" . $key . "/"); 
						break;
					default:
						// not supported
						break;
				}
				break;
			default:
				// not supported
				break;
		}

		break;	
	case "folder" :
		echo "<files>";
		$foldName = $_GET['fn'];
		$result = dbUtil::SELECT("SELECT clmnFileName, clmnFileSize, clmnFileModified, clmnFileType FROM tblfiles WHERE clmnUNFile=:un AND clmnFoldIn=:fn AND clmnEncrypted='0' ", array(':fn' => $foldName ,':un' => $_SESSION['un']));
		if ( count($result) ) { 
			foreach($result as $row) {
				echo "<file>";
				echo "<filename>" . $row['clmnFileName'] . "</filename>";
				echo "<filesize>" . FileSizeConvert($row['clmnFileSize']) . "</filesize>";
				echo "<filemod>" . date("d/m/Y", $row['clmnFileModified']) . "</filemod>";
				$type =  explode("/", $row['clmnFileType']);
				echo "<filetype>" . $type[0]  . "</filetype>";
				echo "</file>";
			}
		} else {
			xml_encode("error", "No files were found!");
		}
		echo "</files>";
		
		echo "<folders>";
		$result = dbUtil::SELECT("SELECT clmnFoldName FROM tblfolders WHERE clmnUNFolder=:un AND clmnFoldIn=:fn ", array(':fn' => $foldName ,':un' => $_SESSION['un']));
		if ( count($result) ) { 
			foreach($result as $row) {
				echo "<folder>";
				echo "<foldername>" . $row['clmnFoldName'] . "</foldername>";
				echo "</folder>";
			}
		} else {
			xml_encode("error", "No folders were found!");
		}
		echo "</folders>";
		break;
	case "shared" :
		echo "<files>";
		$userIdResult = dbUtil::SELECT("SELECT clmnId FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
		if(count($userIdResult) != 0) {
			$userId = $userIdResult[0]['clmnId'];
		} 
		//Select Shared files with the user
		$fileIdResult = dbUtil::SELECT("SELECT clmnFileID FROM tblSharedFiles WHERE clmnUsrID=:usrid", array(':usrid' => $userId));
		if(count($fileIdResult) != 0) {
			foreach($fileIdResult as $row) {
				$fileId = $row['clmnFileID'];
				echo $fileId;
				$fileResult = dbUtil::SELECT("SELECT clmnFileName, clmnFileSize, clmnFileModified, clmnFileType FROM tblFiles WHERE clmnFileID=:fileid", array(':fileid' => $fileId));
				
				if ( count($fileResult) ) { 
					foreach($fileResult as $trow) {
						echo "<file>";
						echo "<filename>" . $trow['clmnFileName'] . "</filename>";
						echo "<filesize>" . FileSizeConvert($trow['clmnFileSize']) . "</filesize>";
						echo "<filemod>" . date("d/m/Y", $trow['clmnFileModified']) . "</filemod>";
						$type =  explode("/", $trow['clmnFileType']);
						echo "<filetype>" . $type[0]  . "</filetype>";
						echo "</file>";
					}
				} else {
					xml_encode("error", "No files were found!");
				}
				
			}
			
		}
		//Select files shared BY the user
		$fileIdResult = dbUtil::SELECT("SELECT clmnFileID FROM tblSharedFiles WHERE clmnFileOwner=:usrid", array(':usrid' => $userId));
		if(count($fileIdResult) != 0) {
			foreach($fileIdResult as $row) {
				$fileId = $row['clmnFileID'];
				echo $fileId;
				$fileResult = dbUtil::SELECT("SELECT clmnFileName, clmnFileSize, clmnFileModified, clmnFileType FROM tblFiles WHERE clmnFileID=:fileid", array(':fileid' => $fileId));
				
				if ( count($fileResult) ) { 
					foreach($fileResult as $trow) {
						echo "<file>";
						echo "<filename>" . $trow['clmnFileName'] . "</filename>";
						echo "<filesize>" . FileSizeConvert($trow['clmnFileSize']) . "</filesize>";
						echo "<filemod>" . date("d/m/Y", $trow['clmnFileModified']) . "</filemod>";
						$type =  explode("/", $trow['clmnFileType']);
						echo "<filetype>" . $type[0]  . "</filetype>";
						echo "</file>";
					}
				} else {
					xml_encode("error", "No files were found!");
				}
				
			}
			
		}
		echo "</files>";
		break;
	case "encrypted" :
		$userIdResult = dbUtil::SELECT("SELECT clmnId FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
		if(count($userIdResult) != 0) {
			$userId = $userIdResult[0]['clmnId'];
		} 

		$fileResult = dbUtil::SELECT("SELECT clmnFileName, clmnFileSize, clmnFileModified, clmnFileType FROM tblFiles WHERE clmnUNFile=:uname AND clmnEncrypted='1'", array(':uname' => $_SESSION['un']));
		
		if ( count($fileResult) ) { 
			foreach($fileResult as $trow) {
				echo "<file>";
				echo "<filename>" . $trow['clmnFileName'] . "</filename>";
				echo "<filesize>" . FileSizeConvert($trow['clmnFileSize']) . "</filesize>";
				echo "<filemod>" . date("d/m/Y", $trow['clmnFileModified']) . "</filemod>";
				$type =  explode("/", $trow['clmnFileType']);
				echo "<filetype>" . $type[0]  . "</filetype>";
				echo "</file>";
			}
		} else {
			xml_encode("error", "No files were found!");
		}
		break;
	default :
		//not supported
		break;
}
	
echo "</flrequest>";
header('Content-Type: application/xml; charset=utf-8');

function Zip($dir, $source, $name)
{
	$zip = new ZipIt();
	$res = $zip->open($dir . '.zip', ZipArchive::CREATE);                 
	if ($res === TRUE) {
	  $zip->addDir($source, basename($source));
	  $zip->close();
	} 

}

class ZipIt extends ZipArchive
{
 
    public function addDir($location, $name)
    {
        $this->addEmptyDir($name);
        $this->addDirDo($location, $name);
    }
 
    private function addDirDo($location, $name)
    {
        $name .= '/';
        $location .= '/';
 
        // Read all Files in Dir
        $dir = opendir($location);
        while ($file = readdir($dir)) {
            if ($file == '.' || $file == '..')
                continue;
 
            $do = (filetype($location . $file) == 'dir') ? 'addDir' : 'addFile';
            echo $name . $file;
            echo $location . $file;
            $this->$do($location . $file, $name . $file);
        }
    }
 
}

function removeDir($path) {
   $i = new DirectoryIterator($path);
        foreach($i as $f) {
            if($f->isFile()) {
                unlink($f->getRealPath());
            } else if(!$f->isDot() && $f->isDir()) {
                rrmdir($f->getRealPath());
            }
        }
        rmdir($path);
    }
?>