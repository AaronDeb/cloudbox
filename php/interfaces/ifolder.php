<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 3/9/2016
 * Time: 9:57 PM
 */

interface IFolder {
    public function getFolderId();
    public function getFolderName();
    public function getFolderOwnerId();
    public function getOwnerID();

    public function setFolderId($id);
    public function setFolderName($name);
    public function setFolderOwnerId($id);
    public function setOwnerID($id);
}

?>