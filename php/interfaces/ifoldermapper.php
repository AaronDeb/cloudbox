<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 3/9/2016
 * Time: 9:59 PM
 */

interface IFolderMapper {
    public function insert(IFolder $folder);
    public function update(IFolder $folder);
    public function traverse(IFolder $folder);
    public function delete($id);
}