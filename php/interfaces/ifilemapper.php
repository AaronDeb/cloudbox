<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 3/5/2016
 * Time: 7:04 PM
 */

interface IFileMapper
{
    public function findById($id);
    public function findByFolderId($id);
    public function insert(IFile $file);
    public function updateFileInfo(IFile $file);
    public function getFileContents(IFile $file);
    public function setFileContents(IFile $file, $content);
    public function delete($id);
}

?>