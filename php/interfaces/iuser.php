<?php

interface IUser
{
    public function getName();
    public function getPassword();
    public function getEmail();
    public function getKey();
    public function getIsAdmin();

    public function setName($name);
    public function setEmail($email);
    public function setPassword($passw) ;
    public function setKey($key) ;
    public function setIsAdmin($isAdmin);
    public function toArray();
}


?>