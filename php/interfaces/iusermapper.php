<?php

interface IUserMapper
{
    public function findById($id);
    public function findByUserName($username);
    public function insert(IUser $user);
    public function update(IUser $user);
    public function delete($id);
}

?>