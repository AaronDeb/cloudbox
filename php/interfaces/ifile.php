<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 3/5/2016
 * Time: 7:04 PM
 */

interface IFile
{
    public function getFileId();
    public function getFileName();
    public function getFileOwner();
    public function getFileSize();
    public function getFileModified();
    public function getFileMimeType();
    public function getFileMappedName();
    public function getFileEncrypted();
    public function getFileParentFolder();

    /* SETTERS */
    public function setFileName($name);
    public function setFileOwner($owner);
    public function setFileSize($size);
    public function setFileModified($modified);
    public function setFileMimeType($mimeType);
    public function setFileMappedName($mappedName);
    public function setFileEncrypted($encrypted);
    public function setFileParentFolder($id);
}

?>