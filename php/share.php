<?PHP
include 'init.php';
session_start();
date_default_timezone_set('Europe/Rome');

$mode = $_GET['mode'];

printXML("shareRequest");
if($mode == 'init')
{
	$time = $_GET['tmstmp'];
	$filename = $_GET['filename'];
	$fileIdResult = dbUtil::SELECT("SELECT clmnFileId FROM tblFiles WHERE clmnFileName=:name AND clmnUNFile=:uname", array(':name' => $filename, ':uname' => $_SESSION['un']));
	if(count($fileIdResult) != 0) {
			$fileId = $fileIdResult[0]['clmnFileId'];
	} else { die(); }
	$userIdResult = dbUtil::SELECT("SELECT clmnId FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
	if(count($userIdResult) != 0) {
		$userId = $userIdResult[0]['clmnId'];
	} else { die(); }
	
	$currUserActivityResult = dbUtil::SELECT("SELECT clmnFwId FROM tblFileWatch WHERE clmnUserId=:unid AND clmnFileId=:fileid", array(':unid' => $userId, ':fileid' => $fileId));
	if(count($currUserActivityResult) == 0) { //Check if I am already watching...
		//(false) Make it look like I'm watching
		$exc = dbUtil::INSERT("INSERT INTO tblFileWatch VALUES('', :clmnfileid, :clmnuserid, :activity)", array(':clmnfileid' => $fileId, ':clmnuserid' => $userId, ':activity' => $time));
		if($exc == true)
		{
			//sucess
		}
	} else {
		//(true) Update my activity
		$exc = dbUtil::UPDATE("UPDATE tblFileWatch SET clmnActivity=:activity WHERE clmnUserId=:unid", array(':unid' => $userId, ':activity' => $time));
		if($exc == true)
		{
			//sucess
		}
	}
	
	$fileShrIdResult = dbUtil::SELECT("SELECT clmnUsrID FROM tblSharedFiles WHERE clmnFileID=:fileid", array(':fileid' => $fileId));
	printXML("shared");
	if(count($fileShrIdResult) != 0) {
		//File is Shared
		echo "true";
	} else {
		//File is not Shared
		echo "false";
		die();
	}
	printXML("/shared");
	
	getWatchers($fileId, $userId, false);
		
	$actFileNameResult = dbUtil::SELECT("SELECT clmnActFN FROM tblFiles WHERE clmnFileId=:fileid", array(':fileid' => $fileId));
	$actFileName = $actFileNameResult[0]['clmnActFN'];
	$fh = fopen("../files/" . $actFileName,'r');
	$fileArray = null;
	$i = 0;
	
	while ($line = fgets($fh)) {
	  $fileArray[$i] = $line;
	  $i++;
	}
	fclose($fh);
	
	printXML("text");
	for ($x = 0; $x <= (sizeof($fileArray)-1); $x++) {
		printXML("line" . $x);
		echo str_replace(array('\n', '
'), '', $fileArray[$x]); 
		printXML("/line" . $x);
	}
	printXML("/text");

} else if($mode == 'activity') {
	$fileName = $_GET['file'];
	$fileIdResult = dbUtil::SELECT("SELECT clmnFileId FROM tblFiles WHERE clmnFileName=:name AND clmnUNFile=:uname", array(':name' => $fileName, ':uname' => $_SESSION['un']));
	if(count($fileIdResult) != 0) {
			$fileId = $fileIdResult[0]['clmnFileId'];
	} else { die(); }
	$userIdResult = dbUtil::SELECT("SELECT clmnId FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
	if(count($userIdResult) != 0) {
		$userId = $userIdResult[0]['clmnId'];
	} else { die(); }		
	
	getWatchers($fileId, $userId, true);
	
} else if($mode == 'action') {
	$actMode = $_GET['action'];
	$fileName = $_GET['file'];
	$lineNo = $_GET['line'];
	$text = $_GET['text'];
	$cursPos = $_GET['pos'];
	$val = validateAction($actMode);
	if($val == true) {
		$fileIdResult = dbUtil::SELECT("SELECT clmnFileId FROM tblFiles WHERE clmnFileName=:name AND clmnUNFile=:uname", array(':name' => $fileName, ':uname' => $_SESSION['un']));
		if(count($fileIdResult) != 0) {
				$fileId = $fileIdResult[0]['clmnFileId'];
		} else { die(); }
		$userIdResult = dbUtil::SELECT("SELECT clmnId FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']));
		if(count($userIdResult) != 0) {
			$userId = $userIdResult[0]['clmnId'];
		} else { die(); }
		
		$exc = dbUtil::INSERT("INSERT INTO tblFileModif VALUES('', :clmnfileid, :clmnuserid, :clmnfileline, :clmnmode, :clmntext, :clmnpos)", array(':clmnfileid' => $fileId, ':clmnuserid' => $userId, ':clmnfileline' => $lineNo, ':clmnmode' => $actMode, ':clmntext' => $text, ':clmnpos' => $curPos));
		if($exc == true)
		{
			//sucess
		}

	}	
	
}
printXML("/shareRequest");

header('Content-Type: application/xml; charset=utf-8');

function getWatchers($fileId, $userId, $delete) {
	$lastActivityResult = dbUtil::SELECT("SELECT clmnUserId, clmnActivity FROM tblFileWatch WHERE clmnFileId=:fileid", array(':fileid' => $fileId));
	if(count($lastActivityResult) != 0) {
		//File has Watchers (Check if there are on-line)

		foreach($lastActivityResult as $row) {
			if($row['clmnUserId'] != $userId)
			{
				$lastActivity = new DateTime($row['clmnActivity']);
				$currentTime = new DateTime(date("m/d/Y H:i:s"));
				$interval = $lastActivity->diff($currentTime);
				///var_dump($currentTime > $lastActivity);
				//echo $interval->d . "d " . $interval->h . "h " . $interval->i . "m " . $interval->s . "s";
				if($currentTime > $lastActivity)
				{
					if(($interval->d + $interval->h ) == 0 && $interval->i <= 1) {
						printXML("watchers");
						
						//active user found!
						$userNameResult = dbUtil::SELECT("SELECT clmnUserName FROM tblUsers WHERE clmnId=:unid", array(':unid' => $row['clmnUserId']));
						echo $userNameResult[0]['clmnUserName'];
						
						printXML("/watchers");
						printUserLocation($row['clmnUserId'], $fileId, $delete);
					}
				}
			}
		}
	} 
}

function validateAction($action) {
	if($action == "write") {
		return true;
	} else if($action == "newline") {
		return true;
	} else if($action == "backspace") {
		return true;
	} else if($action == "delete") {
		return true;
	}
	return false;
}

function printUserLocation($activeUser, $fileId, $delete) {
	
	$userLocationResult = dbUtil::SELECT("SELECT clmnFileLine, clmnLineMode, clmnLineText, clmnLinePos FROM tblFileModif WHERE clmnFileId=:fileid AND clmnUserId=:userid", array(':userid' => $activeUser, ':fileid' => $fileId));
	foreach($userLocationResult as $row) {
		printXML("userloc");
		printXML("line");
		echo $row['clmnFileLine'];
		printXML("/line");
		printXML("mode");
		echo $row['clmnLineMode'];
		printXML("/mode");
		printXML("ltext");
		echo str_replace(array('\n', '\r'), "", $row['clmnLineText']);
		printXML("/ltext");
		printXML("pos");
		echo $row['clmnLinePos'];
		printXML("/pos");
		printXML("/userloc");
	}
	if($delete) {
		$deleteLogsResult = dbUtil::DELETE("DELETE FROM tblFileModif WHERE clmnFileId=:fileid AND clmnUserId=:userid", array(':userid' => $activeUser, ':fileid' => $fileId));
	}
}

function printXML($str) {
	echo "<" . $str . ">";
}


?>