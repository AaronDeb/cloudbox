<?PHP
include 'init.php';
session_start();

$mode = $_GET['get'];

switch ($mode) {
	case 'user':
		echo "<userinfo>";
		if(isset($_SESSION['un']) )
		{
			echo "<user>" . $_SESSION['un'] . "</user>";
		}
		
		$userInfoResult = dbUtil::SELECT("SELECT clmnName, clmnEmail FROM tblUsers WHERE clmnUserName=:uname", array(':uname' => $_SESSION['un']) );
		if(count($userInfoResult) != 0) {
			echo "<name>" . $userInfoResult[0]['clmnName'] . "</name>";
			echo "<email>" . $userInfoResult[0]['clmnEmail'] . "</email>";
		}
		echo "</userinfo>";
		break;
	case 'fileusage' :
		echo "<usagerequest>";

		$allFileSizesResult = dbUtil::SELECT("SELECT clmnFileSize FROM tblFiles WHERE clmnUNFile=:uname", array(':uname' => $_SESSION['un']) );
		$overallusage =0;
		if(count($allFileSizesResult) != 0) {
			foreach($allFileSizesResult as $row) {
				$overallusage += $row['clmnFileSize'];
			}
		}
		echo "<fig>" . FileSizeConvert($overallusage) . "</fig>";

		/*
		52 428 800 = 100%
		usage 	   = ?
		*/
		$uppercal = $overallusage * 100;
		$usage = $uppercal / 52428800;
		echo "<perc>" . (int)$usage . "</perc>";

		echo "</usagerequest>";
		break;
	case 'adminusage' :
		if($_SESSION['un'] == 'admin') {
			$action = $_GET['action'];
			echo "<usagerequest>";

			switch ($action) {
				case 'file':
					
					$allFileSizesResult = dbUtil::SELECT("SELECT clmnFileSize FROM tblFiles", null);
					$overallusage =0;
					if(count($allFileSizesResult) != 0) {
						foreach($allFileSizesResult as $row) {
							$overallusage += $row['clmnFileSize'];
						}
					}

					/*
					52 428 800 = 100%
					usage 	   = ?
					*/
					$uppercal = $overallusage * 100;
					$usage = $uppercal / 52428800;
					echo "<perc>" . (int)$usage . "</perc>";
					
					break;
				case 'cpu':
					echo "<perc>" . (int)_loadavg() . "</perc>";
					break;
				default:
					# code...
					break;
			}

			echo "</usagerequest>";
		}
		break;
	default:
		# code...
		break;
}

header('Content-Type: application/xml; charset=utf-8');

function _loadavg()
{
    if(class_exists("COM")) 
    {
    	$wmi = new COM("WinMgmts:{impersonationLevel=impersonate}") ;
		$cpus = $wmi->ExecQuery("SELECT LoadPercentage FROM Win32_Processor");

		foreach ($cpus as $cpu) 
		{
	       return $cpu->LoadPercentage;
      	}
    } 
}

?>