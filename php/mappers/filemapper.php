<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 3/5/2016
 * Time: 6:59 PM
 */

include "/../interfaces/ifilemapper.php";

class FileMapper implements IFileMapper{
    private $db;
    private $fs;
    private $fileDir = "temp/";
    private $table = "tblfiles";

    public function __construct(IDB $db, IFS $fs) {
        $this->db = $db;
        $this->fs = $fs;
    }

    public function findById($id){
        $res = $this->db->getData( $this->table, ["clmnFileID" => $id] );

        if( sizeof($res) ==0 ) {
            return [];
        }
        return $this->loadFile( $res[0] ); // make this into an instance
    }

    public function findByFolderId($id) {

        $res = $this->db->getData( $this->table, ["clmnFileParentFolderID" => $id] );

        if( sizeof($res) ==0 ) {
            return [];
        }
        return $this->loadFile( $res[0] ); // make this into an instance
    }

    public function insert(IFile $file) {

        $this->db->insertRecord( $this->table, [
            "clmnFileName" => $file->getFileName(),
            "clmnFileOwnerID" => $file->getFileOwner(),
            "clmnFileSize" => $file->getFileSize(),
            "clmnFileModified" => $file->getFileModified(),
            "clmnFileMimeType" => $file->getFileMimeType(),
            "clmnFileMappedName" => $file->getFileMappedName(),
            "clmnEncrypted" => $file->getFileEncrypted(),
            "clmnFileParentFolderID" => $file->getFileParentFolder()
        ] );

    }

    public function updateFileInfo(IFile $file){
        $this->db->updateRecord( $this->table, [
            "clmnID" => $file->getID()
        ],
            [
                "clmnFileName" => $file->getFileName(),
                "clmnFileOwnerID" => $file->getFileOwner(),
                "clmnFileSize" => $file->getFileSize(),
                "clmnFileModified" => $file->getFileModified(),
                "clmnFileMimeType" => $file->getFileMimeType(),
                "clmnFileMappedName" => $file->getFileMappedName(),
                "clmnEncrypted" => $file->getFileEncrypted(),
                "clmnFileParentFolderID" => $file->getFileParentFolder()
            ] );
    }

    public function getFileContents(IFile $file) {
        return $this->fs->readFile($this->fileDir . $file->getFileMappedName());
    }

    public function setFileContents(IFile $file, $content){
        $this->fs->writeFile($this->fileDir . $file->getFileMappedName(), $content);
    }

    public function delete($id){
        $this->db->deleteRecord( $this->table, $id );
    }

    private function loadFile($row) {
        //($fileId, $fileName, $fileOwner, $fileSize, $fileModified, $fileMimeType, $fileMappedName, $fileEncrypted, $parentFolderID )
        return new File(
            $row["clmnFileID"],
            $row["clmnFileName"],
            $row["clmnFileOwnerID"],
            $row["clmnFileSize"],
            $row["clmnFileModified"],
            $row["clmnFileMimeType"],
            $row["clmnFileMappedName"],
            $row["clmnEncrypted"],
            $row["clmnFileParentFolderID"]
        );
    }


}