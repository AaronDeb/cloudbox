<?php

include "/../interfaces/iusermapper.php";

class UserMapper implements IUserMapper {
    private $db;
    private $table = "tblusers";

    public function __construct(IDB $db) {
        $this->db = $db;
    }

    public function findById($id) {
        $res = $this->db->getData( $this->table, ["clmnId" => $id] );
        return $this->loadUser( $res[0] ); // make this into an instance
    }

    public function findByUserName($username) {
        $res = $this->db->getData( $this->table, ["clmnUserName" => $username] );

        if( sizeof($res) ==0 ) {
            return [];
        }
        return $this->loadUser( $res[0] ); // make this into an instance
    }

    public function insert(IUser $user) {
        $existanceCheck = $this->findByUserName($user->getUserName());

        if( sizeof($existanceCheck) == 0) {
            $this->db->insertRecord( $this->table, [
                "clmnName" => $user->getName(),
                "clmnUserName" => $user->getUserName(),
                "clmnPassword" => md5($user->getPassword()),
                "clmnEmail" => $user->getEmail(),
                "clmnKey" => $user->getKey()
            ] );
        } else {
            die("Username already taken!");
        }
    }

    public function update(IUser $user){
        $this->db->updateRecord( $this->table, [
            "clmnID" => $user->getID()
        ],
        [
            "clmnUserName" => $user->getName(),
            "clmnPassword" => $user->getPassword(),
            "clmnEmail" => $user->getEmail()
        ] );
    }

    public function delete($id) {
        $this->db->deleteRecord( $this->table, $id );
    }

    private function loadUser($row) {
        return new User(
            $row["clmnID"],
            $row["clmnUserName"],
            $row["clmnName"],
            $row["clmnPassword"],
            false,
            $row["clmnEmail"],
            $row["clmnKey"]
        );
    }
}

?>