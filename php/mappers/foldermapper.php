<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 3/7/2016
 * Time: 10:24 PM
 */

include "/../interfaces/ifoldermapper.php";

class FolderMapper implements IFolderMapper{
    private $db;
    private $table = "tblfolders";

    public function __construct(IDB $db) {
        $this->db = $db;
    }

    private function findBy( $ownerID, $selector, $selectorValue ){
        $res = $this->db->getData( $this->table, ['clmnFolderOwnerID' => $ownerID, $selector => $selectorValue] );

        if( sizeof($res) ==0 ) {
            return [];
        }
        return $this->loadFile( $res[0] ); // make this into an instance
    }

    public function insert(IFolder $folder) {
        $this->db->insertRecord( $this->table, [
            "clmnFolderID" => $folder->getFolderId(),
            "clmnFolderName" => $folder->getFolderName(),
            "clmnFolderParentID" => $folder->getFolderOwnerId(),
            "clmnFolderOwnerID" => $folder->getOwnerID()
        ] );

    }

    public function traverse(IFolder $folder) {
        $result = $this->db->getData( $this->table, ['clmnFolderParentID' => $folder->getFolderOwnerId(), 'clmnFolderOwnerID' => $folder->getOwnerID()] );
        $newResult = [];
        for( $o = 0; $o <= count($result)-1; $o++ ) {
            array_push($newResult, [
                "id" => $result[$o]["clmnFolderID"],
                "name" => $result[$o]["clmnFolderName"],
                "size" => null,
                "modified" => null,
                "type" => "folder"
            ]);
        }
        return $newResult;
    }

    public function update(IFolder $folder){
        $this->db->updateRecord( $this->table, [
            "clmnFolderID" => $folder->getFolderId()
        ],
            [
                "clmnFolderName" => $folder->getFolderName(),
                "clmnFolderParentID" => $folder->getFolderOwnerId(),
                "clmnFolderOwnerID" => $folder->getOwnerID()
            ]
        );
    }

    public function delete($id){
        $this->db->deleteRecord( $this->table, $id );
    }

}

?>